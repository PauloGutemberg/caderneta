//
//  ConstrutorDeController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit

class ConstrutorDeController<Controller: UIViewController, ParametrosController> {
    
}

extension ConstrutorDeController where Controller == TelaInicialViewController, ParametrosController == TelaInicialViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioEscolaViewController, ParametrosController == FormularioEscolaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == ListaSalasDeAulaViewController, ParametrosController == ListaSalasDeAulaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionarDisciplinaViewController, ParametrosController == FormularioSelecionarDisciplinaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionarAnoViewController, ParametrosController == FormularioSelecionarAnoViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionarTurnoViewController, ParametrosController == FormularioSelecionarTurnoViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSiglaViewController, ParametrosController == FormularioSiglaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == ListaAlunosViewController, ParametrosController == ListaAlunosViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionaGeneroViewController, ParametrosController == FormularioSelecionaGeneroViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionaDataViewController, ParametrosController == FormularioSelecionaDataViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioNomeViewController, ParametrosController == FormularioNomeViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == ListaNotasViewController, ParametrosController == ListaNotasViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionaTipoExportacaoViewController, ParametrosController == FormularioSelecionaTipoExportacaoViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionaPeriodoViewController, ParametrosController == FormularioSelecionaPeriodoViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioTituloNotaViewController, ParametrosController == FormularioTituloNotaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioDescricaoNotaViewController, ParametrosController == FormularioDescricaoNotaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioSelecionaTempoNotaViewController, ParametrosController == FormularioSelecionaTempoNotaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioNotaViewController, ParametrosController == FormularioNotaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == FormularioDataNotaViewController, ParametrosController == FormularioDataNotaViewControllerModel {
    
    static func criaController(_ parametros: ParametrosController, mainCoordinator: MainCoordinator) -> Controller {
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == BoletimViewController, ParametrosController == BoletimViewControllerModel {
    
    static func criaController(_ parametros: BoletimViewControllerModel,mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
}

extension ConstrutorDeController where Controller == EditDeleViewController, ParametrosController == EditDeleViewControllerModel {
    
    static func criaController(_ parametros: EditDeleViewControllerModel, mainCoordinator: MainCoordinator) -> Controller{
        
        let controller = Controller(mainCoordinator: mainCoordinator)
        controller.modalPresentationStyle = .overFullScreen
        controller.parametros = parametros
        return controller
    }
    
}
