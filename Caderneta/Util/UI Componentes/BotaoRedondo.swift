//
//  BotaoRedondo.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 12/12/20.
//

import UIKit
import SnapKit

class BotaoRedondo: UIButton {
    
    var imgView: UIImageView?
    var proporcaoDaImagem: Double = 1.0 {
        didSet {
            guard let imgView = self.imgView else {
                return
            }
            imgView.snp.remakeConstraints { (refazer) in
                refazer.width.height.equalToSuperview().multipliedBy(self.proporcaoDaImagem)
                refazer.center.equalToSuperview()
            }
        }
    }
    
    var imagem: UIImage? {
        didSet {
            if self.imgView == nil {
                let imgView = UIImageView(image: self.imagem)
                imgView.isUserInteractionEnabled = false
                imgView.contentMode = .scaleAspectFit
                self.addSubview(imgView)
                imgView.snp.makeConstraints { (fazer) in
                    fazer.width.height.equalToSuperview().multipliedBy(self.proporcaoDaImagem)
                    fazer.center.equalToSuperview()
                }
                self.imgView = imgView
            }else{
                self.imgView?.image = self.imagem
            }
        }
    }
    
    override var bounds: CGRect {
        didSet{
            guard bounds.width > 0 else {
                return
            }
            self.layer.cornerRadius = bounds.width/2
        }
    }
    
}


