//
//  ImagemViewRedonda.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/12/20.
//

import UIKit

class ImagemViewRedonda: UIImageView {
    
    override func layoutSubviews() {
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        guard self.frame.width > 0 else { return }
        self.layer.cornerRadius = self.frame.width/2
    }
}
