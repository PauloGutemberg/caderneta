//
//  Enums.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/11/20.
//

import UIKit

enum SelecionadorDeCor {
    
    //MARK: modoClaro_modoEscuro
    case preto_branco
    
    //MARK: sem o modo dark
    case barraDeNavegacao
    case botaoProsseguir
    case selecionarDisciplina
    case notaAzul
    case notaVermelha
    case resultadoFinalMensagem
    case bolinhaPeriodoSeguimento
    
    var cor: UIColor {
        switch self {
        
        case .preto_branco:
            return UIColor(named: "lightBlueSecondary") ?? UIColor(hex: "01377F")
        case .barraDeNavegacao:
            return UIColor(named: "navBar") ?? UIColor(hex: "01377F")
        case .botaoProsseguir:
            return UIColor(named: "botaoProsseguir") ?? UIColor(hex: "01377F")
        case .selecionarDisciplina:
            return UIColor(named: "selecaoDisciplina") ?? UIColor(hex: "01377F")
        case .notaAzul:
            return UIColor(named: "notaAzul") ?? UIColor(hex: "01377F")
        case .notaVermelha:
            return UIColor(named: "notaVermelha") ?? UIColor(hex: "01377F")
        case .resultadoFinalMensagem:
            return UIColor(named: "resultadoFinalMensagem") ?? UIColor(hex: "01377F")
        case .bolinhaPeriodoSeguimento:
            return UIColor(named: "bolinhaPeriodoSeguimento") ?? UIColor(hex: "01377F")
        }
    }
}

enum GeneroDescricao: String {
    case masculino = "MASCULINO"
    case feminino = "FEMININO"
}

enum TipoCelulaListaNotas {
    case detalhamento
    case resultadoFinal
}

enum PeriodoDescricao: String {
    case primeiro = "1ºP"
    case segundo = "2ºP"
    case terceiro = "3ºP"
    case quarto = "4ºP"
    case recuperacao = "RECUPERAÇÃO"
}

enum SeguimentoNotaDescricao: String {
    case primeiro = "1º Nota"
    case segundo = "2º Nota"
    case terceiro = "3º Nota"
    case quarto = "4º Nota"
    case recuperacao = "Recuperação/Período"
}
