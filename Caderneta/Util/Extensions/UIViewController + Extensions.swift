//
//  UIViewController + Extensions.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/12/20.
//

import UIKit

extension UIViewController {
    
    private static var _imagemParaTitulo: UIImage?
    var imagemParaTitulo: UIImage? {
        get {
            return UIViewController._imagemParaTitulo
        }
        
        set{
            UIViewController._imagemParaTitulo = newValue
        }
    }
    
    private static var _titulo: String?
    var titulo: String? {
        get{
            return UIViewController._titulo
        }
        
        set{
            UIViewController._titulo = newValue
        }
    }
}
