//
//  String + Extensions.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import UIKit

extension NSMutableAttributedString {
    
    @discardableResult func normal(_ text: String, font: UIFont? = UIFont.systemFont(ofSize: 12.0), color: UIColor? = nil, linkUrl: String? = nil) -> NSMutableAttributedString {
        var attrs: [NSAttributedString.Key: Any] = [.font: font!]
        if let url = linkUrl { attrs[.link] = url }
        if let colorText = color { attrs[.foregroundColor] = colorText }
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        
        append(normal)
        
        return self
    }
}
