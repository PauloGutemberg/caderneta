//
//  UIView + Extensions.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit

extension CodableView {
    func setupView(){
        setupComponents()
        buildViews()
        setupConstraints()
    }
}

extension UIView {
        
    func comoImagem() -> UIImage{
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
