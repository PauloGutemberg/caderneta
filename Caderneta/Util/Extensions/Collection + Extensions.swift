//
//  Collection + Extensions.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/12/20.
//

import UIKit


extension Collection {
    
    /// Retorna o elemento no índice especificado se estiver dentro dos limites, caso contrário, nil.
    subscript (seguro index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Array where Element: Disciplina {
    func disciplinasSelecionadas() -> [Element]? {
        return self.filter { (disciplina) -> Bool in
            return disciplina.estaSelecionada
        }
    }
    
    func Ids() -> [String] {
        var ids = Array<String>()
        self.forEach { (disciplina) in
            ids.append(disciplina.id)
        }
        return ids
    }
}

extension Array where Element: Ano {
    func anoSelecionado() -> Element? {
        return self.first { (ano) -> Bool in
            return ano.estaSelecionado
        }
    }
}

extension Array where Element: Turno {
    func turnoSelecionado() -> Element? {
        return self.first { (turno) -> Bool in
            return turno.estaSelecionado
        }
    }
}

extension Array where Element: Periodo {
    func periodoSelecionado() -> Element? {
        return self.first { (periodo) -> Bool in
            return periodo.estaSelecionado
        }
    }
}

extension Array where Element: TempoNota {
    func tempoNotaSelecionado() -> Element? {
        return self.first { (tempoNota) -> Bool in
            return tempoNota.estaSelecionado
        }
    }
}
