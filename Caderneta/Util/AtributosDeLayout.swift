//
//  AtributosDeLayout.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 11/12/20.
//

import UIKit

struct AtributosDeLayout {
    
    private static let larguraDeReferencia: CGFloat = 375
    
    static let margemPadrao       : CGFloat = UIScreen.main.bounds.width * (16/larguraDeReferencia)
    static let espacamentoSimples : CGFloat = UIScreen.main.bounds.width * (4/larguraDeReferencia)
    static let espacamentoDuplo   : CGFloat = UIScreen.main.bounds.width * (8/larguraDeReferencia)
    
    static func capturaValorResponsivo(_ valor: CGFloat) -> CGFloat {
        return UIScreen.main.bounds.width * (valor/larguraDeReferencia)
    }
}

struct ConstraintPriority {
    
    static let lowPriority  : CGFloat = 250
    static let highPriority : CGFloat = 900
}
