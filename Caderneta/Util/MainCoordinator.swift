//
//  MainCoordinator.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit

final class MainCoordinator {
    
    var controleDeNavegacao: ControleDeNavegacaoCustomizado?
    var janela: UIWindow
    
    init(janela: UIWindow) {
        self.janela = janela
    }
    
    private func abreComoRootViewController(controller: UIViewController){
        self.janela.rootViewController?.dismiss(animated: false, completion: nil)
        self.janela.makeKeyAndVisible()
        self.janela.rootViewController = controller
    }
    
    //MARK: inicio das chamadas de telas
    
    func abreTelaInicialViewController(){
        let controller = ConstrutorDeController<TelaInicialViewController, TelaInicialViewControllerModel>.criaController(TelaInicialViewControllerModel(), mainCoordinator: self)
        abreComoRootViewController(controller: controller)
    }
    
    func abreListaDeEscolas(){
        let tabBarController = TabBarViewController(mainCoordinator: self)
        let navController = ControleDeNavegacaoCustomizado()
        self.janela.rootViewController = navController
        navController.present(tabBarController, animated: true){
            guard let controleDeNavegacao = tabBarController.viewControllers?.first as? ControleDeNavegacaoCustomizado else { return }
            controleDeNavegacao.responder = self.controleDeNavegacao?.responder
            self.controleDeNavegacao = controleDeNavegacao
        }
    }
    
    func abreFormularioAdicionarEscola<T: UIViewController & FormularioEscolaViewControllerResponder>(_ parametros: FormularioEscolaViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioEscolaViewController, FormularioEscolaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true, completion: nil)
    }
    
    func abreListaDeSalasDeAula(_ parametros: ListaSalasDeAulaViewControllerModel){
        let controller = ConstrutorDeController<ListaSalasDeAulaViewController, ListaSalasDeAulaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        self.controleDeNavegacao?.pushViewController(controller, animated: true)
    }
    
    func abreFormularioSelecionarDisciplina<T: UIViewController & FormularioSelecionarDisciplinaViewControllerResponder>(_ parametros: FormularioSelecionarDisciplinaViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionarDisciplinaViewController, FormularioSelecionarDisciplinaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true, completion: nil)
    }
    
    func abreFormularioSelecionarAno<T: UIViewController & FormularioSelecionarAnoViewControllerResponder>(_ parametros: FormularioSelecionarAnoViewControllerModel, chamador: T) {
        let controller = ConstrutorDeController<FormularioSelecionarAnoViewController, FormularioSelecionarAnoViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true, completion: nil)
    }
    
    func abreFormularioSelecionaTurno<T: UIViewController & FormularioSelecionarTurnoViewControllerResponder>(_ parametros: FormularioSelecionarTurnoViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionarTurnoViewController, FormularioSelecionarTurnoViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true, completion: nil)
    }
    
    func abreFormularioAdicionarSigla<T: UIViewController & FormularioSiglaViewControllerResponder>(_ parametros: FormularioSiglaViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSiglaViewController, FormularioSiglaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true, completion: nil)
    }
    
    func abreTelaListaDeAlunos(_ parametros: ListaAlunosViewControllerModel){
        let controller = ConstrutorDeController<ListaAlunosViewController, ListaAlunosViewControllerModel>.criaController(parametros, mainCoordinator: self)
        self.controleDeNavegacao?.pushViewController(controller, animated: true)
    }
    
    func abreFormularioSelecionaGenero<T: UIViewController & FormularioSelecionaGeneroViewControllerResponder>(_ parametros: FormularioSelecionaGeneroViewControllerModel, chamador: T){
        
        let controller = ConstrutorDeController<FormularioSelecionaGeneroViewController, FormularioSelecionaGeneroViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioSelecionaData<T: UIViewController & FormularioSelecionaDataViewControllerResponder>(_ parametros: FormularioSelecionaDataViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionaDataViewController, FormularioSelecionaDataViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioNome<T: UIViewController & FormularioNomeViewControllerResponder>(_ parametros: FormularioNomeViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioNomeViewController, FormularioNomeViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreTelaListaDeNotas(_ parametros: ListaNotasViewControllerModel){
        let controller = ConstrutorDeController<ListaNotasViewController, ListaNotasViewControllerModel>.criaController(parametros, mainCoordinator: self)
        self.controleDeNavegacao?.pushViewController(controller, animated: true)
    }
    
    func abreFormularioSelecionaTipoExportacao<T: UIViewController & FormularioSelecionaTipoExportacaoViewControllerResponder>(_ parametros: FormularioSelecionaTipoExportacaoViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionaTipoExportacaoViewController, FormularioSelecionaTipoExportacaoViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioSelecionaPeriodo<T: UIViewController & FormularioSelecionaPeriodoViewControllerResponder>(_ parametros: FormularioSelecionaPeriodoViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionaPeriodoViewController, FormularioSelecionaPeriodoViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioTituloNota<T: UIViewController & FormularioTituloNotaViewControllerResponder>(_ parametros: FormularioTituloNotaViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioTituloNotaViewController, FormularioTituloNotaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioDescricaoNota<T: UIViewController & FormularioDescricaoNotaViewControllerResponder>(_ parametros: FormularioDescricaoNotaViewControllerModel, chamador: T){
        
        let controller = ConstrutorDeController<FormularioDescricaoNotaViewController, FormularioDescricaoNotaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioTempoNota<T: UIViewController & FormularioSelecionaTempoNotaViewControllerResponder>(_ parametros: FormularioSelecionaTempoNotaViewControllerModel, chamador: T){
        let controller = ConstrutorDeController<FormularioSelecionaTempoNotaViewController, FormularioSelecionaTempoNotaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioNota<T: UIViewController & FormularioNotaViewControllerResponder>(_ parametros: FormularioNotaViewControllerModel, chamador: T){
        
        let controller = ConstrutorDeController<FormularioNotaViewController, FormularioNotaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreFormularioDataNota<T: UIViewController & FormularioSelecionaDataViewControllerResponder>(_ parametros: FormularioDataNotaViewControllerModel, chamador: T){
        
        let controller = ConstrutorDeController<FormularioDataNotaViewController, FormularioDataNotaViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
    
    func abreTelaBoletim(){
        let controller = ConstrutorDeController<BoletimViewController, BoletimViewControllerModel>.criaController(BoletimViewControllerModel() ,mainCoordinator: self)
        self.controleDeNavegacao?.pushViewController(controller, animated: true)
    }
    
    func abreTelaEditDele(_ parametros: EditDeleViewControllerModel, chamador: UIViewController){
        let controller = ConstrutorDeController<EditDeleViewController, EditDeleViewControllerModel>.criaController(parametros, mainCoordinator: self)
        chamador.present(controller, animated: true)
    }
}

extension MainCoordinator: Coordinator {
    func iniciar() {
        abreTelaInicialViewController()
    }
}
