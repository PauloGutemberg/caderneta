//
//  Protocolos.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import Foundation

//MARK: Coordinator

protocol Coordinator {
    func iniciar()
}

//MARK: DismissCallback

protocol BaseViewControllerDismissCallback {
    func recebeValor(_ valor: Any?, sender: Any?)
}

//MARK: View

public protocol CodableView: class {
    func setupComponents()
    func buildViews()
    func setupConstraints()
}

//MARK: Responder

protocol ListaViewResponder {
    func adicionarPressionado()
}

protocol FormularioEscolaViewControllerResponder {
    func botaoProsseguirPressionado()
}

protocol FormularioEscolaViewResponder {
    func botaoProsseguirPressionado(texto: String)
    func botaoFecharPressionado()
}

protocol FormularioSelecionarDisciplinaViewResponder {
    func botaoProsseguirPressionado()
    func botaoFecharPressionado()
}

protocol FormularioSelecionarAnoViewControllerResponder {
    func botaoProsseguirAoSelecionarAnoPressionado()
}

protocol FormularioSelecionarAnoViewResponder {
    func botaoProsseguirPressionado()
    func botaoFecharPressionado()
}

protocol FormularioSelecionarTurnoViewControllerResponder {
    func botaoProsseguirAoSelecionarTurnoPressionado()
}

protocol FormularioSelecionarTurnoViewResponder {
    func botaoProsseguirPressionado()
    func botaoFecharPressionado()
}

protocol FormularioSiglaViewControllerResponder {
    func botaoProsseguirAoEscreverUmaSiglaPressionado()
}

protocol FormularioSiglaViewResponder {
    func botaoProsseguirPressionado(texto: String)
    func botaoFecharPressionado()
}

protocol ListaAlunosViewResponder {
    func adicionarPressionado()
}

protocol FormularioSelecionaGeneroViewResponder {
    func botaoProsseguirPressionado(genero: Genero)
    func botaoFecharPressionado()
}

protocol FormularioSelecionaGeneroViewControllerResponder {
    func botaoProsseguirAoSelecionarGeneroPressionado()
}

protocol FormularioSelecionaDataViewResponder {
    func botaoProsseguirPressionado(data: String)
    func botaoFecharPressionado()
}

protocol FormularioSelecionaDataViewControllerResponder {
   func botaoProsseguirAoSelecionarDataPressionado()
}

protocol FormularioNomeViewResponder {
    func botaoProsseguirPressionado(texto: String)
    func botaoFecharPressionado()
}

protocol FormularioNomeViewControllerResponder {
   func botaoProsseguirAoEscreverUmNomePressionado()
}

protocol ListaNotasViewResponder {
    func abaSelecionada(aba: Int)
}

protocol BotaoAdicionarNotaViewResponder {
    func botaoAdicionarNotaPressionado()
}

protocol BotaoExportarBoletimViewResponder {
    func botaoExportarBoletimPressionado()
}

protocol FormularioSelecionaTipoExportacaoViewResponder {
    func botaoProsseguirPressionado(tipoExportacao: String)
    func botaoFecharPressionado()
}

protocol FormularioSelecionaTipoExportacaoViewControllerResponder {
    func botaoProsseguirAoSelecionarTipoExportacaoPressionado()
}

protocol FormularioSelecionaPeriodoViewResponder {
    func botaoProsseguirPressionado()
    func botaoFecharPressionado()
}

protocol FormularioSelecionaPeriodoViewControllerResponder {
    func botaoProsseguirAoSelecionarPeriodoPressionado()
}

protocol FormularioTituloNotaViewControllerResponder {
    func botaoProsseguirAoEscreverUmTituloPressionado()
}

protocol FormularioTituloNotaViewResponder {
    func botaoProsseguirPressionado(texto: String)
    func botaoFecharPressionado()
}

protocol FormularioDescricaoNotaViewControllerResponder {
    func botaoProsseguirAoEscreverUmaDescricaoPressionado()
}

protocol FormularioDescricaoNotaViewResponder {
    func botaoProsseguirPressionado(texto: String)
    func botaoFecharPressionado()
}

protocol FormularioSelecionaTempoNotaViewControllerResponder {
    func botaoProsseguirAoSelecionarTempoNotaPressionado()
}

protocol FormularioSelecionaTempoNotaViewResponder {
    func botaoProsseguirPressionado()
    func botaoFecharPressionado()
}

protocol FormularioNotaViewControllerResponder {
    func botaoProsseguirAoAdicionarNotaPressionado()
}

protocol FormularioSelecionarDisciplinaViewControllerResponder {
    func botaoProsseguirAoSelecionarDisciplinaPressionado()
}

//MARK: Presenter
protocol FormularioSelecionarDisciplinaOutput {
    func retornaDisciplinas(disciplinas: [Disciplina])
}

protocol FormularioSelecionarAnoOutput {
    func retornarAnos(anos: [Ano])
}

protocol FormularioSelecionarTurnoOutput {
    func retornaTurnos(turnos: [Turno])
}

protocol ListaEscolasOutput {
    func retornaEscolas(escolas: [Escola])
}

protocol ListaTurmasOutput {
    func retornaTurmas(turmas: [Turma])
    func retornaEscolaAssociada(escola: Escola)
}

protocol ListaAlunosOutput {
    func retornaAlunos(alunos: [Aluno])
    func retornaTurmaAssociada(turma: Turma)
}

protocol FormularioSelecionaGeneroOutput {
    func retornaGeneros(generos: [Genero])
}

protocol ListaNotasOutput {
    func retornaDisciplinas(disciplinas: [Disciplina])
    func retornaNotas(notas: [Nota], chavePaginaNotasAssociada: String)
    func retornaResultadoFinal(resultado: ResultadoFinal, chavePaginaNotasAssociada: String)
}

protocol FormularioSelecionaPeriodoOutput {
    func retornaPeriodos(periodos: [Periodo])
}

protocol FormularioSelecionaTempoNotaOutput {
    func retornaTempoNota(tempoNotas: [TempoNota])
}

protocol FormularioEscolaOutput {
    
}

protocol FormularioSiglaOutput {
    
}

protocol FormularioNomeOutput {
    
}

protocol FormularioDataNotaOutput {
    func obterParticipacao(participacao: ParticipacaoNaTurma)
}

//MARK: Servicos
protocol ServicoConsultaEscolasOutput {
    func obterEscolas(escolas: [Escola])
    func obterEscola(escola: Escola)
}

protocol ServicoConsultaTurmasOutput {
    func obterTurmas(turmas: [Turma])
    func obterTurma(turma: Turma)
}

protocol ServicoConsultaDisciplinasOutput {
    func obterDisciplinas(disciplinas: [Disciplina])
    func obterDisciplina(disciplina: Disciplina)
}

protocol ServicoConsultaAlunosOutput {
    func obterAlunos(alunos: [Aluno])
}

protocol ServicoConsultaParticipacaoTurmaOutput {
    func obterParticipacoes(participacoes: [ParticipacaoNaTurma])
    func obterParticipacao(participacao: ParticipacaoNaTurma)
}

protocol ServicoConsultaNotaOutput {
    func obterNotas(notas: [Nota])
}
