//
//  AlunoProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/01/21.
//

import Foundation
import RealmSwift

final class AlunoProvedor: Provedor {
        
    typealias T = Aluno
    
    var valores: [Aluno] {
        get {
            let realm = try! Realm()
            let realmAluno = realm.objects(AlunoRealm.self)
            let alunos = Array(realmAluno.map {
                Aluno.init(realmObjeto: $0)
            })
            return alunos
        }
        
        set {
            let realm = try! Realm()
            let todos = realm.objects(AlunoRealm.self)
            
            try! realm.write {
                realm.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: Aluno) {
        let aluno = AlunoRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(aluno)
        }
    }
    
    func deletar(_ object: Aluno) {
        let aluno = AlunoRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(AlunoRealm.self).filter("id=%@", aluno.id))
        }
    }
    
    func atualizar(_ object: Aluno) {
        let aluno = AlunoRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(AlunoRealm.self).filter("id=%@", aluno.id))
        }
        
        try! realm.write {
            realm.add(aluno)
        }
    }
    
    func obter(_ withID: String) -> Aluno? {
        let realm = try! Realm()
        let objects = realm.objects(AlunoRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    Aluno.init(realmObjeto: $0)
        })).first
    }


}
