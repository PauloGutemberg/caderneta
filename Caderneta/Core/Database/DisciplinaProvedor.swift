//
//  DisciplinaProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation
import RealmSwift

final class DisciplinaProvedor: Provedor {
        
    typealias T = Disciplina
    
    var valores: [Disciplina] {
        get {
            let realm = try! Realm()
            let realmDisciplina = realm.objects(DisciplinaRealm.self)
            let disciplinas = Array(realmDisciplina.map {
                Disciplina.init(realmObjeto: $0)
            })
            return disciplinas
        }
        
        set {
            let realm = try! Realm()
            let todos = realm.objects(DisciplinaRealm.self)
            
            try! realm.write {
                realm.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: Disciplina) {
        let disciplina = DisciplinaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(disciplina)
        }
    }
    
    func deletar(_ object: Disciplina) {
        let disciplina = DisciplinaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(DisciplinaRealm.self).filter("id=%@", disciplina.id))
        }
    }
    
    func atualizar(_ object: Disciplina) {
        let disciplina = DisciplinaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(DisciplinaRealm.self).filter("id=%@", disciplina.id))
        }
        
        try! realm.write {
            realm.add(disciplina)
        }
    }
    
    func obter(_ withID: String) -> Disciplina? {
        let realm = try! Realm()
        let objects = realm.objects(DisciplinaRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    Disciplina.init(realmObjeto: $0)
        })).first
    }


}
