//
//  NotaProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/01/21.
//

import Foundation
import RealmSwift


final class NotaProvedor: Provedor {
    
    typealias T = Nota
    
    var valores: [Nota] {
        get {
            let realm = try! Realm()
            let realmNota = realm.objects(NotaRealm.self)
            let notas = Array(realmNota.map {
                Nota.init(realmObjeto: $0)
            })
            return notas
        }
        
        set {
            let realm = try! Realm()
            let todos = realm.objects(NotaRealm.self)
            
            try! realm.write {
                realm.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: Nota) {
        let nota = NotaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(nota)
        }
    }
    
    func deletar(_ object: Nota) {
        let nota = NotaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(NotaRealm.self).filter("id=%@", nota.id))
        }
    }
    
    func atualizar(_ object: Nota) {
        let nota = NotaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(NotaRealm.self).filter("id=%@", nota.id))
        }
        
        try! realm.write {
            realm.add(nota)
        }
    }
    
    func obter(_ withID: String) -> Nota? {
        let realm = try! Realm()
        let objects = realm.objects(NotaRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    Nota.init(realmObjeto: $0)
        })).first
    }
}
