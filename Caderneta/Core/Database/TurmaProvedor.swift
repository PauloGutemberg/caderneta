//
//  TurmaProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation
import RealmSwift

final class TurmaProvedor: Provedor {
    
    typealias T = Turma
    
    var valores: [Turma] {
        get {
            let realm = try! Realm()
            let realmTurma = realm.objects(TurmaRealm.self)
            let turmas = Array(realmTurma.map {
                Turma.init(realmObjeto: $0)
            })
            return turmas
        }
        
        set {
            let realm = try! Realm()
            let todos = realm.objects(TurmaRealm.self)
            
            try! realm.write {
                realm.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: Turma) {
        let turma = TurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(turma)
        }
    }
    
    func deletar(_ object: Turma) {
        let turma = TurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(TurmaRealm.self).filter("id=%@", turma.id))
        }
    }
    
    func atualizar(_ object: Turma) {
        let turma = TurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(TurmaRealm.self).filter("id=%@", turma.id))
        }
        
        try! realm.write {
            realm.add(turma)
        }
    }
    
    func obter(_ withID: String) -> Turma? {
        let realm = try! Realm()
        let objects = realm.objects(TurmaRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    Turma.init(realmObjeto: $0)
        })).first
    }

}
