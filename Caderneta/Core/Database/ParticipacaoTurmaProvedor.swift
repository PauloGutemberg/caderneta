//
//  ParticipacaoTurmaProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/01/21.
//

import Foundation
import RealmSwift

final class ParticipacaoTurmaProvedor: Provedor {
        
    typealias T = ParticipacaoNaTurma
    
    var valores: [ParticipacaoNaTurma] {
        get {
            let realm = try! Realm()
            let realmParticipacaoNaTurma = realm.objects(ParticipacaoNaTurmaRealm.self)
            let participacoesNaTurma = Array(realmParticipacaoNaTurma.map {
                ParticipacaoNaTurma.init(realmObjeto: $0)
            })
            return participacoesNaTurma
        }
        
        set {
            let realm = try! Realm()
            let todos = realm.objects(ParticipacaoNaTurmaRealm.self)
            
            try! realm.write {
                realm.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: ParticipacaoNaTurma) {
        let participacaoNaTurma = ParticipacaoNaTurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(participacaoNaTurma)
        }
    }
    
    func deletar(_ object: ParticipacaoNaTurma) {
        let participacaoNaTurma = ParticipacaoNaTurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(ParticipacaoNaTurmaRealm.self).filter("id=%@", participacaoNaTurma.id))
        }
    }
    
    func atualizar(_ object: ParticipacaoNaTurma) {
        let participacaoNaTurma = ParticipacaoNaTurmaRealm.com(objetoAplicacao: object)
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(ParticipacaoNaTurmaRealm.self).filter("id=%@", participacaoNaTurma.id))
        }
        
        try! realm.write {
            realm.add(participacaoNaTurma)
        }
    }
    
    func obter(_ withID: String) -> ParticipacaoNaTurma? {
        let realm = try! Realm()
        let objects = realm.objects(ParticipacaoNaTurmaRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    ParticipacaoNaTurma.init(realmObjeto: $0)
        })).first
    }
    
    func obter(idTurma: String, idDisciplina: String, idAluno: String) -> ParticipacaoNaTurma? {
        let realm = try! Realm()
        let predicado = NSPredicate(format: "idTurma=%@ AND idDisciplina=%@ AND idAluno=%@",idTurma, idDisciplina, idAluno)
        let objetos = realm.objects(ParticipacaoNaTurmaRealm.self).filter(predicado)
        return Array(objetos.map({
                    ParticipacaoNaTurma.init(realmObjeto: $0)
        })).first
    }

}
