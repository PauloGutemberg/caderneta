//
//  EscolaProvedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 10/01/21.
//

import Foundation
import RealmSwift

final class EscolaProvedor: RealmProvedor {
    
    typealias T = Escola
    var fonte: Realm
    
    init(fonte: Realm = try! Realm()){
        self.fonte = fonte
    }
    
    var valores: [Escola] {
        get {
            
            let realmEscola = self.fonte.objects(EscolaRealm.self)
            let escolas = Array(realmEscola.map {
                Escola.init(realmObjeto: $0)
            })
            return escolas
        }
        
        set {
            
            let todos = self.fonte.objects(EscolaRealm.self)
            
            try! self.fonte.write {
                self.fonte.delete(todos)
            }
            newValue.forEach({ inserir($0) })
        }
    }
    
    func inserir(_ object: Escola) {
        let escola = EscolaRealm.com(objetoAplicacao: object)
        
        
        try! self.fonte.write {
            self.fonte.add(escola)
        }
    }
    
    func deletar(_ object: Escola) {
        let escola = EscolaRealm.com(objetoAplicacao: object)
        
        
        try! self.fonte.write {
            self.fonte.delete(self.fonte.objects(EscolaRealm.self).filter("id=%@", escola.id))
        }
    }
    
    func atualizar(_ object: Escola) {
        let escola = EscolaRealm.com(objetoAplicacao: object)
        
        
        try! self.fonte.write {
            self.fonte.delete(self.fonte.objects(EscolaRealm.self).filter("id=%@", escola.id))
        }
        
        try! self.fonte.write {
            self.fonte.add(escola)
        }
    }
    
    func obter(_ withID: String) -> Escola? {
        
        let objects = self.fonte.objects(EscolaRealm.self).filter("id=%@", withID)
        
        return Array(objects.map({
                    Escola.init(realmObjeto: $0)
        })).first
    }
}
