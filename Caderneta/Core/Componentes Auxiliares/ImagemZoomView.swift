//
//  ImagemZoomView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/01/21.
//

import UIKit

class ImagemZoomView: UIView, CodableView {
    
    var scrollView: UIScrollView
    var imagemView: UIImageView
    
    override init(frame: CGRect) {
        self.scrollView = UIScrollView()
        self.imagemView = UIImageView()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.isUserInteractionEnabled = true
        self.isMultipleTouchEnabled = true
        
        let doisToquesGesto = UITapGestureRecognizer(target: self, action: #selector(doisToques))
        doisToquesGesto.numberOfTouchesRequired = 2
        self.scrollView.addGestureRecognizer(doisToquesGesto)
        
        self.scrollView.minimumZoomScale = 1
        self.scrollView.maximumZoomScale = 3.5
        self.scrollView.delegate = self
        
        self.imagemView.contentMode = .scaleAspectFit
        self.imagemView.layer.shadowColor = UIColor.black.withAlphaComponent(0.75).cgColor
        self.imagemView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.imagemView.layer.shadowOpacity = 0.4
        self.imagemView.layer.shadowRadius = 4
    }
    
    func buildViews() {
        self.scrollView.addSubview(self.imagemView)
        self.addSubview(self.scrollView)
    }
    
    func setupConstraints() {
        self.scrollView.snp.makeConstraints { (make) in
            make.top.bottom.trailing.leading.equalToSuperview()
        }
        
        self.imagemView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
        }
    }
    
    @objc func doisToques(){
        let valorZoom: CGFloat = self.scrollView.zoomScale > self.scrollView.minimumZoomScale ? self.scrollView.minimumZoomScale: self.scrollView.maximumZoomScale
        self.scrollView.setZoomScale(valorZoom, animated: true)
    }
    
    func populaView(imagem: UIImage){
        self.imagemView.image = imagem
        self.imagemView.layoutIfNeeded()
    }
}

extension ImagemZoomView: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imagemView
    }
}
