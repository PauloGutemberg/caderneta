//
//  DetalhamentoListaNotasCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import UIKit

class DetalhamentoListaNotasCell: UITableViewCell, CodableView {
    
    static let identificador = "DetalhamentoListaNotasCell"
    
    let container: UIView
    let labelTitulo: UILabel
    let labelSubtitulo: UILabel
    let setinha: UIImageView
    
    var labelValor: UILabel
    var linhaAuxiliar: UIView
    var viewBolinha: UIView
    
    var labelBolinha: UILabel
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.container = UIView()
        self.setinha = UIImageView()
        self.labelTitulo = UILabel()
        self.labelSubtitulo = UILabel()
        
        self.labelValor = UILabel()
        self.linhaAuxiliar = UIView()
        self.viewBolinha = UIView()
        self.labelBolinha = UILabel()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        selectionStyle = .none
        
        self.setinha.contentMode = .scaleAspectFit
        self.setinha.image = #imageLiteral(resourceName: "setinha-baixo")
        
        self.linhaAuxiliar.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.labelTitulo.numberOfLines = 0
        self.labelSubtitulo.numberOfLines = 0
        self.labelValor.textAlignment = .right
        self.labelValor.textColor = UIColor.black
        
        self.viewBolinha.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(13)
        self.viewBolinha.layer.borderColor = UIColor.black.cgColor
        self.viewBolinha.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(0.5)
        self.viewBolinha.backgroundColor = SelecionadorDeCor.bolinhaPeriodoSeguimento.cor
        self.labelBolinha.font = UIFont.regular(size: 13)
    }
    
    func buildViews() {
        self.addSubview(self.linhaAuxiliar)
        self.viewBolinha.addSubview(self.labelBolinha)
        self.addSubview(self.viewBolinha)
        self.addSubview(self.labelValor)
        
        self.container.addSubview(self.labelTitulo)
        self.container.addSubview(self.labelSubtitulo)
        self.container.addSubview(self.setinha)
        addSubview(self.container)
    }
    
    func setupConstraints() {
        viewBolinha.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(26))
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(26))
            make.leading.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.centerY.equalTo(container.snp.top).offset(AtributosDeLayout.capturaValorResponsivo(24))
        }
        
        linhaAuxiliar.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(1))
            make.top.bottom.equalToSuperview()
            make.centerX.equalTo(viewBolinha)
            make.centerY.equalTo(container)
        }
        
        container.snp.makeConstraints { (make) in
            make.leading.equalTo(linhaAuxiliar.snp.trailing).offset(AtributosDeLayout.capturaValorResponsivo(10))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12)).priority(999)
            make.bottom.equalToSuperview()
        }
        
        labelTitulo.snp.remakeConstraints { (make) in
            make.top.leading.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(216))
        }
        
        labelSubtitulo.snp.remakeConstraints { (make) in
            make.top.equalTo(labelTitulo.snp.bottom).inset(AtributosDeLayout.margemPadrao * 2.2)
            make.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.leading.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
            make.trailing.lessThanOrEqualToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(42))
        }
        
        setinha.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(12))
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(8))
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
        }
        
        labelValor.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(72))
            make.trailing.equalTo(setinha.snp.leading).offset(AtributosDeLayout.capturaValorResponsivo(-6))
            make.top.equalTo(labelTitulo.snp.top)
        }
        
        labelBolinha.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    func populaCelula(nota: Nota){
        
        self.labelBolinha.text = "\(nota.periodo).\(nota.seguimento)"
        self.labelValor.text =  String(format: "%.2f", nota.valor)
        configuraCorNota(valor: nota.valor)
        
        UIView.animate(withDuration: 0.01) {
            self.setinha.transform = CGAffineTransform.identity
        }
        self.labelTitulo.attributedText = NSMutableAttributedString().normal("\(nota.titulo)\n", font: UIFont.regular(size: 16), color: UIColor.black)
        if nota.colapso  {
            self.labelSubtitulo.attributedText = NSMutableAttributedString().normal("\(nota.observacao) \(nota.dataNota)", font: UIFont.regular(size: 12), color: UIColor.black)
            UIView.animate(withDuration: 0.25) {
                self.setinha.transform = CGAffineTransform(rotationAngle: .pi)
            }
        }else{
            self.labelSubtitulo.text = ""
        }
        self.layoutIfNeeded()
    }

    func configuraCorNota(valor: Double){
        if valor >= 7.0 {
            self.labelValor.textColor = SelecionadorDeCor.notaAzul.cor
        }else{
            self.labelValor.textColor = SelecionadorDeCor.notaVermelha.cor
        }
    }
    
}
