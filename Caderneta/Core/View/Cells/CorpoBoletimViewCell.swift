//
//  CorpoBoletimViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/02/21.
//

import UIKit

class CorpoBoletimViewCell: UITableViewCell, CodableView {
    
    static var identificadorReusavel: String = "CorpoBoletimViewCell"
    var collectionView: UICollectionView
    var resultados: [String] = []
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())

        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.white
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        self.collectionView.register(CabecalhoBoletimViewCell.self, forCellWithReuseIdentifier: CabecalhoBoletimViewCell.identificador)
        self.collectionView.backgroundColor = .white
        self.collectionView.bounces = false
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.alwaysBounceVertical = true
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func buildViews() {
        self.addSubview(self.collectionView)
    }
    
    func setupConstraints() {
        self.collectionView.snp.makeConstraints { (make) in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(collectionView.snp.width).multipliedBy(0.125)
        }
    }
    
    func populaResultados(){
        self.resultados = ["a","b","c","d","e","f","g","h","i","j","k","l"]
        self.collectionView.layoutSubviews()
        self.collectionView.layoutIfNeeded()
        self.collectionView.reloadData()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    /*override func prepareForReuse() {
        super.prepareForReuse()
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.resultados = []
    }*/

}

extension CorpoBoletimViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.resultados.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CabecalhoBoletimViewCell.identificador, for: indexPath) as? CabecalhoBoletimViewCell else {
            return UICollectionViewCell()
        }
        cell.configuraCelula(com: self.resultados[indexPath.row], indice: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: AtributosDeLayout.capturaValorResponsivo(-120), left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width * 0.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}
