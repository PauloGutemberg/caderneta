//
//  CabecalhoBoletimViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/02/21.
//

import UIKit

class CabecalhoBoletimViewCell: UICollectionViewCell, CodableView {
    
    static var identificador: String = "CabecalhoBoletimViewCell"
    var viewSeparador: UIView
    var lbDescricao: UILabel
    
    override init(frame: CGRect) {
        self.viewSeparador = UIView()
        self.lbDescricao = UILabel()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.lbDescricao.font = UIFont.bold(size: 12)
        self.lbDescricao.textColor = .black
        self.lbDescricao.textAlignment = .center
        self.lbDescricao.numberOfLines = 0
        
        self.viewSeparador.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func buildViews() {
        self.contentView.addSubview(self.lbDescricao)
        self.contentView.addSubview(self.viewSeparador)
    }
    
    func setupConstraints() {
        self.lbDescricao.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(90))
            make.center.equalToSuperview()
        }
        
        self.viewSeparador.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(1))
            make.leading.bottom.top.equalToSuperview()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.viewSeparador.isHidden = false
    }
    
    func configuraCelula(com texto: String, indice: Int){
        self.lbDescricao.text = texto
        if indice == 0 {
            self.viewSeparador.isHidden = true
        }
    }
    
}
