//
//  ResultadoFinalListaNotasCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/01/21.
//

import UIKit

class ResultadoFinalListaNotasCell: UITableViewCell, CodableView {
    
    static var identificador: String = "ResultadoFinalListaNotasCell"
    
    var labelDisciplina: UILabel
    var vwFundo: UIView
    var labelNota: UILabel
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.labelDisciplina = UILabel()
        self.vwFundo = UIView()
        self.labelNota = UILabel()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        selectionStyle = .none
        self.labelDisciplina.numberOfLines = 0
        self.labelDisciplina.font = UIFont.regular(size: 14)
        self.labelNota.numberOfLines = 0
        self.labelNota.textAlignment = .right
        self.labelNota.textColor = UIColor.black
        
        self.vwFundo.layer.borderWidth = 1
        self.vwFundo.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
        self.vwFundo.layer.cornerRadius = 4
    }
    
    func buildViews() {
        self.vwFundo.addSubview(self.labelDisciplina)
        self.vwFundo.addSubview(self.labelNota)
        self.addSubview(vwFundo)
    }
    
    func setupConstraints() {
        
        self.vwFundo.snp.makeConstraints { (fazer) in
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.height.equalTo(AtributosDeLayout.capturaValorResponsivo(56))
            fazer.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
        }
        
        self.labelDisciplina.snp.makeConstraints { (fazer) in
            fazer.leading.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.centerY.equalToSuperview()
        }
        
        self.labelNota.snp.makeConstraints { (fazer) in
            fazer.width.equalTo(AtributosDeLayout.capturaValorResponsivo(72))
            fazer.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.centerY.equalToSuperview()
        }
    }
    
    func populaCelula(disciplina: Disciplina, nota: Nota){
        
        self.labelDisciplina.text = disciplina.nome
        if nota.valor.isNaN {
            self.labelNota.text = "0.0"
        }else{
            self.labelNota.text = String(format: "%.2f", nota.valor)
        }
        configuraCorNota(valor: nota.valor)
    }
    
    func configuraCorNota(valor: Double){
        if valor > 7.0 {
            self.labelNota.textColor = SelecionadorDeCor.notaAzul.cor
        }else{
            self.labelNota.textColor = SelecionadorDeCor.notaVermelha.cor
        }
    }

}
