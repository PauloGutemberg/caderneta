//
//  SelecaoTipoExportacaoCollectionViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/01/21.
//

import UIKit

class SelecaoTipoExportacaoCollectionViewCell: UICollectionViewCell, CodableView {
    
    static var identificador: String = "SelecaoTipoExportacaoCollectionViewCell"
    
    var lbDescricao: UILabel
    var viewSelecao: UIView
    var viewSeparador: UIView
    var stackViewComponentes: UIStackView
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                definirEstadoSelecionado()
            }else{
                definirEstadoDesselecionado()
            }
        }
    }
    
    override init(frame: CGRect) {
        self.viewSelecao = UIView()
        self.viewSeparador = UIView()
        self.lbDescricao = UILabel()
        self.stackViewComponentes = UIStackView(arrangedSubviews: [self.lbDescricao])
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.white
        
        self.lbDescricao.font = UIFont.bold(size: 12)
        self.lbDescricao.textColor = .black
        self.lbDescricao.textAlignment = .center
        
        self.viewSelecao.layer.cornerRadius = frame.height/2
        
        self.viewSeparador.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    
        self.stackViewComponentes.axis = .horizontal
        self.stackViewComponentes.distribution = .fillProportionally
        self.stackViewComponentes.spacing = 2
    }
    
    func buildViews() {
        self.viewSelecao.addSubview(self.stackViewComponentes)
        self.contentView.addSubview(self.viewSelecao)
        self.contentView.addSubview(self.viewSeparador)
    }
    
    func setupConstraints() {
        self.lbDescricao.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        self.viewSelecao.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(100))
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(30))
            make.center.equalToSuperview()
        }
        
        self.stackViewComponentes.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(120))
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(28))
            make.center.equalToSuperview()
        }
        
        self.viewSeparador.snp.makeConstraints { (make) in
            make.width.equalTo(AtributosDeLayout.capturaValorResponsivo(1))
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(28))
            make.leading.equalToSuperview()
            make.bottom.equalTo(viewSelecao.snp.bottom)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.viewSeparador.isHidden = false
    }
    
    func configuraCelula(tipoExportacao: String, indice: Int){
        self.lbDescricao.text = tipoExportacao
        if indice == 0 {
            self.viewSeparador.isHidden = true
        }
    }
    
    func definirEstadoSelecionado(){
        self.viewSelecao.backgroundColor = SelecionadorDeCor.selecionarDisciplina.cor.withAlphaComponent(0.5)
        self.lbDescricao.textColor = UIColor.black
    }
    
    func definirEstadoDesselecionado(){
        self.viewSelecao.backgroundColor = UIColor.clear
        self.lbDescricao.textColor = UIColor.black
    }
}
