//
//  SelecaoImagemCollectionViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 29/12/20.
//

import UIKit

class SelecaoImagemCollectionViewCell: UICollectionViewCell, CodableView {

    static var identificador: String = "SelecaoImagemCollectionViewCell"
    var lbDescricao: UILabel
    var imgView: UIImageView
    var viewSelecao: UIView
    
    override var isSelected: Bool {
        didSet{
            if self.isSelected {
                definirEstadoSelecionado()
            }else{
               definirEstadoDesselecionado()
            }
        }
    }
    
    override init(frame: CGRect) {
        self.lbDescricao = UILabel()
        self.imgView = UIImageView()
        self.viewSelecao = UIView()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(2)
        self.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        
        self.lbDescricao.font = UIFont.bold(size: 12)
        self.lbDescricao.textAlignment = .center
        self.lbDescricao.numberOfLines = 0
    }
    
    func buildViews() {
        self.addSubview(self.lbDescricao)
        self.addSubview(self.imgView)
        self.addSubview(self.viewSelecao)
    }
    
    func setupConstraints() {
        self.imgView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.85)
        }
        
        self.lbDescricao.snp.makeConstraints { (make) in
            make.top.equalTo(self.imgView.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        
        self.viewSelecao.snp.makeConstraints { (make) in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    func populaCelula(genero: Genero){
        self.lbDescricao.text = genero.descricao
        self.imgView.image = UIImage(named: genero.caminhoImagem)
    }
    
    func definirEstadoSelecionado(){
        self.viewSelecao.backgroundColor = SelecionadorDeCor.selecionarDisciplina.cor.withAlphaComponent(0.5)
    }
    
    func definirEstadoDesselecionado(){
        self.viewSelecao.backgroundColor = UIColor.clear
    }
}
