//
//  ListaTableViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 11/12/20.
//

import UIKit
import SnapKit

class ListaTableViewCell: UITableViewCell, CodableView {
    
    static var identificadorReusavel: String = "ListaTableViewCell"
    
    //MARK: declaracoes de variaveis
    var imagemViewPerfil: BotaoRedondo
    var labelTitulo: UILabel
    var labelSubtitulo: UILabel
    var labelDescricaoDetalhada: UILabel
    var stackViewTitulos: UIStackView
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.imagemViewPerfil = BotaoRedondo()
        self.labelTitulo = UILabel()
        self.labelSubtitulo = UILabel()
        self.labelDescricaoDetalhada = UILabel()
        self.stackViewTitulos = UIStackView(arrangedSubviews: [self.labelTitulo, self.labelSubtitulo])
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Metodos

    func setupComponents() {
        self.contentView.isUserInteractionEnabled = false
        self.stackViewTitulos.axis = .vertical
        self.stackViewTitulos.distribution = .fillProportionally
        self.stackViewTitulos.spacing = 0
        
        self.labelTitulo.font = UIFont.semibold(size: 17)
        self.labelTitulo.textColor = .black
        
        self.labelSubtitulo.font = UIFont.regular(size: 15)
        self.labelSubtitulo.textColor = .black
        self.labelSubtitulo.numberOfLines = 0
        self.labelSubtitulo.adjustsFontSizeToFitWidth = true
        self.labelSubtitulo.minimumScaleFactor = 0.75
        
        self.labelDescricaoDetalhada.font = UIFont.semibold(size: 11)
        self.labelDescricaoDetalhada.textColor = .black
        
        self.imagemViewPerfil.proporcaoDaImagem = 1.0
        self.imagemViewPerfil.imgView?.clipsToBounds = true
        self.imagemViewPerfil.clipsToBounds = true
        self.imagemViewPerfil.contentMode = .scaleAspectFill
        self.imagemViewPerfil.addTarget(self, action: #selector(imagemViewPerfilPressionada), for: .touchUpInside)
        
        self.imagemViewPerfil.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(1)
        self.imagemViewPerfil.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        
    }
    
    func buildViews() {
        self.addSubview(self.imagemViewPerfil)
        self.addSubview(self.stackViewTitulos)
        self.addSubview(self.labelDescricaoDetalhada)
    }
    
    func setupConstraints() {
        self.imagemViewPerfil.snp.makeConstraints { (fazer) in
            fazer.height.equalToSuperview().multipliedBy(0.8)
            fazer.height.equalTo(self.imagemViewPerfil.snp.width)
            fazer.leading.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.centerY.equalToSuperview()
        }
        
        self.stackViewTitulos.snp.makeConstraints { (fazer) in
            fazer.leading.equalTo(self.imagemViewPerfil.snp.trailing).offset(AtributosDeLayout.margemPadrao)
            fazer.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.centerY.equalToSuperview()
            fazer.height.equalToSuperview().multipliedBy(0.74)
        }
    
        self.labelDescricaoDetalhada.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.centerY.equalTo(self.imagemViewPerfil)
        }
    }
    
    func populaCelula(escola: Escola){
        
        self.labelTitulo.text = escola.nome
        if let caminhoImagem = escola.caminhoImagem {
            self.imagemViewPerfil.imagem = UIImage(named: caminhoImagem)
        }else{
            self.imagemViewPerfil.imagem = UIImage(named: "fachada-escola")
        }
    }
    
    func populaCelula(turma: Turma){
        self.labelTitulo.text = turma.sigla
        self.labelSubtitulo.text = turma.ano.descricao
        self.labelDescricaoDetalhada.text = turma.turno.descricao
        if let caminhoImagem = turma.caminhoImagem {
            self.imagemViewPerfil.imagem = UIImage(named: caminhoImagem)
        }else{
            self.imagemViewPerfil.imagem = UIImage(named: "sala-de-aula-2")
        }
    }
    
    func populaCelula(aluno: Aluno){
        self.labelTitulo.text = aluno.nome
        self.labelSubtitulo.text = aluno.dataNascimento
        if let caminhoImagem = aluno.caminhoImagem {
            self.imagemViewPerfil.imagem = UIImage(named: caminhoImagem)
        }else{
            if aluno.genero == "MASCULINO" {
                self.imagemViewPerfil.imagem = UIImage(named: "menino")
            }else if aluno.genero == "FEMININO" {
                self.imagemViewPerfil.imagem = UIImage(named: "menina")
            }
        }
    }
    
    @objc func imagemViewPerfilPressionada(){
        
    }
    
}
