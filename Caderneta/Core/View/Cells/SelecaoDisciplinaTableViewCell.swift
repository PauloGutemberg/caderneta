//
//  SelecaoDisciplinaTableViewCell.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 15/12/20.
//

import UIKit

class SelecaoDisciplinaTableViewCell: UITableViewCell, CodableView {
    
    static var identificadorReusavel: String = "SelecaoDisciplinaTableViewCell"
    
    var lbDescricao: UILabel
    var vwFundo: UIView
    var viewSelecaoCelula: UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            configuraSelecionado()
        }else{
            configuraDeselecionado()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.lbDescricao = UILabel()
        self.vwFundo = UIView()
        self.viewSelecaoCelula = UIView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.contentView.isUserInteractionEnabled = false
        self.selectionStyle = .none
        
        self.lbDescricao.numberOfLines = 0
    
        self.vwFundo.layer.borderWidth = 1
        self.vwFundo.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
        self.vwFundo.layer.cornerRadius = 4
        
        self.viewSelecaoCelula.backgroundColor = .clear
        self.viewSelecaoCelula.layer.cornerRadius = 4
        
    }
    
    func buildViews() {
        self.vwFundo.addSubview(self.lbDescricao)
        self.vwFundo.addSubview(self.viewSelecaoCelula)
        self.addSubview(self.vwFundo)
    }
    
    func setupConstraints() {
        
        self.lbDescricao.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.centerY.equalToSuperview()
        }
        
        self.vwFundo.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(56))
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
        }
        
        self.viewSelecaoCelula.snp.makeConstraints { (make) in
            make.leading.top.bottom.trailing.equalToSuperview()
        }
    }
    
    func populaCelula(disciplina: Disciplina){
        self.lbDescricao.text = disciplina.nome
    }
    
    func populaCelula(ano: Ano){
        self.lbDescricao.text = ano.descricao
    }
    
    func populaCelula(turno: Turno){
        self.lbDescricao.text = turno.descricao
    }
    
    func populaCelula(periodo: Periodo){
        self.lbDescricao.text = periodo.descricao
    }
    
    func populaCelula(tempoNota: TempoNota){
        self.lbDescricao.text = tempoNota.descricao
    }
    
    func configuraSelecionado(){
        self.viewSelecaoCelula.backgroundColor = SelecionadorDeCor.selecionarDisciplina.cor.withAlphaComponent(0.5)
        self.layoutIfNeeded()
    }
    
    func configuraDeselecionado(){
        self.viewSelecaoCelula.backgroundColor = .clear
        self.layoutIfNeeded()
    }
}
