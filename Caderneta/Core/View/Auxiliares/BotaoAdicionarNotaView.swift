//
//  BotaoAdicionarNotaView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/01/21.
//

import UIKit

class BotaoAdicionarNotaView: UIView, CodableView {
    
    var botaoAdicionar: UIButton
    var responder: BotaoAdicionarNotaViewResponder?
    
    override init(frame: CGRect) {
        self.botaoAdicionar = UIButton()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        
        self.botaoAdicionar.backgroundColor = SelecionadorDeCor.barraDeNavegacao.cor
        self.botaoAdicionar.setTitle("Adicionar Nota", for: .normal)
        self.botaoAdicionar.setTitleColor(.white, for: .normal)
        self.botaoAdicionar.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.botaoAdicionar.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(1)
        
        self.botaoAdicionar.addTarget(self, action: #selector(botaoAdicionarPressionado), for: .touchUpInside)
    }
    
    func buildViews() {
        self.addSubview(self.botaoAdicionar)
    }
    
    func setupConstraints() {
        self.botaoAdicionar.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.bottom.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.6)
        }
    }
    
    @objc func botaoAdicionarPressionado(){
        self.responder?.botaoAdicionarNotaPressionado()
    }
}
