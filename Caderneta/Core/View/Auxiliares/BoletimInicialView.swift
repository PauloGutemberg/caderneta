//
//  BoletimInicialView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/01/21.
//

import UIKit

class BoletimInicialView: UIView, CodableView {
    
    var tableView: UITableView
    var viewCabecalho: CabecalhoBoletimView
    var viewSeparador: UIView
    var disciplinas: [Disciplina] = [Disciplina(nome: "Portugues", estaSelecionada: false, id: ""), Disciplina(nome: "Matematica", estaSelecionada: false, id: "")]
    
    override init(frame: CGRect) {
        self.tableView = UITableView()
        self.viewSeparador = UIView()
        self.viewCabecalho = CabecalhoBoletimView()
        super.init(frame: frame)
        setupView()
    }
    
    init(){
        self.tableView = UITableView()
        self.viewCabecalho = CabecalhoBoletimView()
        self.viewSeparador = UIView()
        super.init(frame: CGRect.zero)
        self.frame = CGRect(x: 0, y: 0, width: AtributosDeLayout.capturaValorResponsivo(1400), height: AtributosDeLayout.capturaValorResponsivo(470))
        setupView()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(1)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(CorpoBoletimViewCell.self, forCellReuseIdentifier: CorpoBoletimViewCell.identificadorReusavel)
        
        self.viewSeparador.backgroundColor = .black
    }
    
    func buildViews() {
        self.addSubview(self.viewCabecalho)
        self.addSubview(self.viewSeparador)
        self.addSubview(self.tableView)
    }
    
    func setupConstraints() {
        self.viewCabecalho.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(50))
        }
        
        self.viewSeparador.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.viewCabecalho.snp.bottom)
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(1))
        }
        
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(viewSeparador.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension BoletimInicialView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.disciplinas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: CorpoBoletimViewCell.identificadorReusavel, for: indexPath) as? CorpoBoletimViewCell else {
            return UITableViewCell()
        }
        cell.populaResultados()
        //cell.text = "oi eu sou igris"
      return cell
    }
}
