//
//  ListaNotasView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import UIKit

class ListaNotasView: UIView, CodableView {
    
    var paginasDisciplinas = [(chave: String, tableView: UITableView)]()
    var responder: ListaNotasViewResponder?
    
    var collectionView: UICollectionView
    var scrollView: UIScrollView
    var linhaSeparadora: UIView
    
    override init(frame: CGRect) {
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.scrollView = UIScrollView()
        self.linhaSeparadora = UIView()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.white
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        
        self.collectionView.register(ListaNotasCabecalhoViewCell.self, forCellWithReuseIdentifier: ListaNotasCabecalhoViewCell.identificador)
        self.collectionView.backgroundColor = .white
        self.collectionView.bounces = false
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.alwaysBounceVertical = true
        
        self.linhaSeparadora.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        self.scrollView.isPagingEnabled = true
        self.scrollView.panGestureRecognizer.delaysTouchesBegan = true
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.delegate = self
    }
    
    func buildViews() {
        addSubview(self.collectionView)
        addSubview(self.linhaSeparadora)
        addSubview(self.scrollView)
    }
    
    func setupConstraints() {
        self.collectionView.snp.makeConstraints { (make) in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(collectionView.snp.width).multipliedBy(0.125)
        }
        
        self.linhaSeparadora.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom)
            make.width.equalToSuperview()
            make.height.equalTo(1)
        }
       
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(linhaSeparadora.snp.bottom)
            make.trailing.leading.bottom.equalToSuperview()
        }
    }
    
    func criaPaginaDisciplina(chaveAssociada: String) {
        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        self.scrollView.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(linhaSeparadora.snp.bottom)
            make.bottom.equalTo(self)
            make.leading.equalToSuperview().inset(UIScreen.main.bounds.width * CGFloat(self.paginasDisciplinas.count))
            make.width.equalTo(UIScreen.main.bounds.width)
        }
        self.paginasDisciplinas.append((chaveAssociada, tableView))
        scrollView.contentSize = CGSize(width : UIScreen.main.bounds.width * CGFloat(self.paginasDisciplinas.count),
                                                 height: 0)
    }
    
    func retornaTableviewPaginaDisciplina(chaveAssociada: String) -> UITableView? {
        return self.paginasDisciplinas.first(where: { (pagina) -> Bool in
            pagina.chave.lowercased() == chaveAssociada.lowercased()
        })?.tableView
    }
}

extension ListaNotasView: UIScrollViewDelegate {
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let parteInteira = Int(scrollView.contentOffset.x/scrollView.frame.size.width)
        let parteDecimal = scrollView.contentOffset.x/scrollView.frame.size.width - CGFloat(parteInteira)
        
        let indexPaginaAtual = parteDecimal > 0.5 ? parteInteira + 1 : parteInteira
        guard floor(parteDecimal) == parteDecimal else { return }
        self.responder?.abaSelecionada(aba: indexPaginaAtual)
    }
    
    func scrollToPage(page: Int) {
        self.scrollView.contentOffset.x = UIScreen.main.bounds.width * CGFloat(page)
    }
    
    func isAbaSelecionada(aba: Int) -> Bool {
        return scrollView.contentOffset.x == UIScreen.main.bounds.width * CGFloat(aba)
    }
}
