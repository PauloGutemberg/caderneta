//
//  ListaAlunosView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import UIKit
import SnapKit

class ListaAlunosView: UIView, CodableView {
    
    var tableView: UITableView
    var botaoAdicionar: UIButton
    var cabecalhoView: UIView
    
    var responder: ListaAlunosViewResponder?
    
    override init(frame: CGRect) {
        self.cabecalhoView = UIView()
        self.botaoAdicionar = UIButton()
        self.tableView = UITableView()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = .white
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        self.tableView.tableHeaderView = UIView(frame: frame)
        self.tableView.tableFooterView = UIView(frame: frame)
        
        self.cabecalhoView.backgroundColor = SelecionadorDeCor.barraDeNavegacao.cor
        self.cabecalhoView.layer.cornerRadius = 30
        
        self.botaoAdicionar.backgroundColor = .clear
        self.botaoAdicionar.setTitle("Adicionar Aluno", for: .normal)
        self.botaoAdicionar.setTitleColor(.white, for: .normal)
        self.botaoAdicionar.layer.borderColor = UIColor.white.cgColor
        self.botaoAdicionar.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.botaoAdicionar.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(1)
        
        self.botaoAdicionar.addTarget(self, action: #selector(botaoAdicionarPressionado), for: .touchUpInside)
    }
    
    func buildViews() {
        self.cabecalhoView.addSubview(self.botaoAdicionar)
        self.addSubview(self.cabecalhoView)
        self.addSubview(self.tableView)
    }
    
    func setupConstraints() {
        
        self.cabecalhoView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(-22))
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(80))
        }
        
        self.botaoAdicionar.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.height.equalToSuperview().multipliedBy(0.4)
        }
        
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.cabecalhoView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    @objc func botaoAdicionarPressionado(){
        responder?.adicionarPressionado()
    }

    
}
