//
//  CabecalhoBoletimView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/02/21.
//

import UIKit

class CabecalhoBoletimView: UIView, CodableView {
    
    var collectionView: UICollectionView
    var linhaSeparadora: UIView
    var cabecalho: [String] = ["COMPONENTES CURRICULARES","1ºP","REC","2ºP","REC","3ºP","REC","4ºP","MÉDIA ANUAL","RECUP FINAL","MEDIA FINAL"]
    
    override init(frame: CGRect) {
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.linhaSeparadora = UIView()
        super.init(frame: frame)
    }
    
    
    init(){
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.linhaSeparadora = UIView()
        super.init(frame: CGRect.zero)
        self.frame = CGRect(x: 0, y: 0, width: AtributosDeLayout.capturaValorResponsivo(1400), height: AtributosDeLayout.capturaValorResponsivo(60))
        setupView()
        self.collectionView.reloadData()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.white
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        self.collectionView.register(CabecalhoBoletimViewCell.self, forCellWithReuseIdentifier: CabecalhoBoletimViewCell.identificador)
        self.collectionView.backgroundColor = .white
        self.collectionView.bounces = false
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.alwaysBounceVertical = true
        
        self.linhaSeparadora.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

    }
    
    func buildViews() {
        self.addSubview(self.collectionView)
        self.addSubview(self.linhaSeparadora)
    }
    
    func setupConstraints() {
        self.collectionView.snp.makeConstraints { (make) in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(collectionView.snp.width).multipliedBy(0.125)
        }
        
        self.linhaSeparadora.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom)
            make.width.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}

extension CabecalhoBoletimView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cabecalho.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CabecalhoBoletimViewCell.identificador, for: indexPath) as? CabecalhoBoletimViewCell else {
            return UICollectionViewCell()
        }
        
        cell.configuraCelula(com: self.cabecalho[indexPath.row], indice: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: AtributosDeLayout.capturaValorResponsivo(-120), left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width * 0.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
