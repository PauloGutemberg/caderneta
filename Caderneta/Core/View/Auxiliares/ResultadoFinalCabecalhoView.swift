//
//  ResultadoFinalCabecalhoView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/01/21.
//

import UIKit

class ResultadoFinalCabecalhoView: UIView, CodableView {
    
    var vwMae: UIView
    var labelDescricao: UILabel
    
    override init(frame: CGRect) {
        self.vwMae = UIView()
        self.labelDescricao = UILabel()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.labelDescricao.font = UIFont.regular(size: 14)
        self.labelDescricao.numberOfLines = 0
        self.vwMae.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(4)
        self.vwMae.backgroundColor = SelecionadorDeCor.resultadoFinalMensagem.cor
    }
    
    func buildViews() {
        self.vwMae.addSubview(self.labelDescricao)
        self.addSubview(self.vwMae)
    }
    
    func setupConstraints() {
        self.vwMae.snp.makeConstraints { (fazer) in
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.top.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.bottom.equalToSuperview()
        }
        
        self.labelDescricao.snp.makeConstraints { (fazer) in
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            fazer.center.equalToSuperview()
        }
    }
    
    func populaView(mensagem: String){
        self.labelDescricao.text = mensagem
    }
}
