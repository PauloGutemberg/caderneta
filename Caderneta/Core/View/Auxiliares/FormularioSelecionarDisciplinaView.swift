//
//  FormularioSelecionarDisciplinaView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/12/20.
//

import UIKit

class FormularioSelecionarDisciplinaView: UIView, CodableView {
    
    var viewPrincipal: UIView
    var botaoFechar: UIButton
    
    var lbTitulo: UILabel
    var lbSubtitulo: UILabel
    var stackViewCabecalho: UIStackView
    var tableView: UITableView
    var vwBackgroundTableView: UIView
    
    var botaoCadastrarDisciplinasSelecionada: UIButton
    
    var responder: FormularioSelecionarDisciplinaViewResponder?
    
    override init(frame: CGRect) {
        
        self.viewPrincipal = UIView()
        self.botaoFechar = UIButton()
        
        self.lbTitulo = UILabel()
        self.lbSubtitulo = UILabel()
        self.stackViewCabecalho = UIStackView(arrangedSubviews: [self.lbTitulo, self.lbSubtitulo])
        
        self.tableView = UITableView()
        self.vwBackgroundTableView = UIView()
        self.botaoCadastrarDisciplinasSelecionada = UIButton()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.viewPrincipal.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.viewPrincipal.backgroundColor = .white
        
        self.stackViewCabecalho.axis = .vertical
        self.stackViewCabecalho.spacing = AtributosDeLayout.capturaValorResponsivo(8)
        self.lbTitulo.text = "Disciplinas"
        self.lbTitulo.textColor = .black
        self.lbTitulo.font = UIFont.boldSystemFont(ofSize: 22)
        
        self.lbSubtitulo.text = "Selecione as disciplinas necessarias para compor está turma"
        self.lbSubtitulo.textColor = .black
        self.lbSubtitulo.font = UIFont.regular(size: 12)
        self.lbSubtitulo.numberOfLines = 0
        
        self.botaoFechar.setImage(UIImage(named: "fechar"), for: .normal)
        self.botaoFechar.addTarget(self, action: #selector(botaoFecharPressionado), for: .touchUpInside)
                
        self.botaoCadastrarDisciplinasSelecionada.setTitle("PROSSEGUIR", for: .normal)
        self.botaoCadastrarDisciplinasSelecionada.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.botaoCadastrarDisciplinasSelecionada.setTitleColor(.black, for: .normal)
        self.botaoCadastrarDisciplinasSelecionada.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        self.botaoCadastrarDisciplinasSelecionada.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.botaoCadastrarDisciplinasSelecionada.isEnabled = false
        self.botaoCadastrarDisciplinasSelecionada.addTarget(self, action: #selector(botaoProsseguirPressionado), for: .touchUpInside)
        
        self.tableView.allowsMultipleSelection = true
    }
    
    func buildViews() {
        self.viewPrincipal.addSubview(self.botaoFechar)
        self.viewPrincipal.addSubview(self.stackViewCabecalho)
        self.viewPrincipal.addSubview(self.botaoCadastrarDisciplinasSelecionada)
        self.vwBackgroundTableView.addSubview(self.tableView)
        self.viewPrincipal.addSubview(self.vwBackgroundTableView)
        self.addSubview(self.viewPrincipal)
    }
    
    func setupConstraints() {
        
        self.viewPrincipal.snp.makeConstraints { (make) in
            make.top.equalTo(AtributosDeLayout.capturaValorResponsivo(UIScreen.main.bounds.height * 0.2))
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(AtributosDeLayout.capturaValorResponsivo(8))
        }
        
        self.botaoFechar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.height.width.equalTo(AtributosDeLayout.margemPadrao * 2)
        }
        
        stackViewCabecalho.snp.makeConstraints { (fazer) in
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
            fazer.top.equalTo(self.botaoFechar.snp.bottom).offset(AtributosDeLayout.capturaValorResponsivo(16))
        }
        
        lbSubtitulo.snp.makeConstraints { (fazer) in
            fazer.height.equalTo(AtributosDeLayout.capturaValorResponsivo(72))
        }
        
        tableView.snp.makeConstraints { (fazer) in
            fazer.top.leading.trailing.bottom.equalToSuperview()
        }
        
        vwBackgroundTableView.snp.makeConstraints { (fazer) in
            fazer.top.equalTo(stackViewCabecalho.snp.bottom)
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
            fazer.bottom.equalTo(botaoCadastrarDisciplinasSelecionada.snp.top)
        }
      
        botaoCadastrarDisciplinasSelecionada.snp.makeConstraints { (fazer) in
            fazer.height.equalTo(AtributosDeLayout.capturaValorResponsivo(50))
            fazer.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(40))
            fazer.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
        }
    }
    
    @objc func botaoFecharPressionado(){
        self.responder?.botaoFecharPressionado()
    }
    
    @objc func botaoProsseguirPressionado(){
        self.responder?.botaoProsseguirPressionado()
    }
    
    func definirStatusBotaoAoRemoverTodasAsSelecoesDeDisciplina(){
        self.botaoCadastrarDisciplinasSelecionada.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.botaoCadastrarDisciplinasSelecionada.isEnabled = false
    }
    
    func definirStatusBotaoAoSelecionarAoMenosUmaDisciplina() {
        self.botaoCadastrarDisciplinasSelecionada.backgroundColor = SelecionadorDeCor.botaoProsseguir.cor
        self.botaoCadastrarDisciplinasSelecionada.isEnabled = true
    }
    
}
