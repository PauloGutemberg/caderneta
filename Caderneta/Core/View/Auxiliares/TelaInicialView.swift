//
//  TelaInicialView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit
import SnapKit

class TelaInicialView: UIView, CodableView {
    
    var vwTeste: UIView
    
    override init(frame: CGRect) {
        self.vwTeste = UIView()
        
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.vwTeste.backgroundColor = .blue
        print("1")
    }
    
    func buildViews() {
        addSubview(self.vwTeste)
        print("2")
    }
    
    func setupConstraints() {
        self.vwTeste.snp.makeConstraints { (make) in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        print("3")
    }
    
}

