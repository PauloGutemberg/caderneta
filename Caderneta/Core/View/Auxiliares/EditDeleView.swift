//
//  EditDeleView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 28/02/21.
//

import UIKit

protocol EditDeleViewResponder {
    func botaoFecharPressionado()
    func botaoEditarPressionado()
}

class EditDeleView: UIView, CodableView {
    
    var viewPrincipal: UIView
    var botaoFechar: UIButton
    var lbTitulo: UILabel
    var iconeImageView: UIImageView
    var labelTitulo: UILabel
    var botaoEditar: UIButton
    
    var responder: EditDeleViewResponder?
    
    override init(frame: CGRect) {
        self.viewPrincipal = UIView()
        self.botaoFechar = UIButton()
        self.lbTitulo = UILabel()
        self.iconeImageView = UIImageView()
        self.labelTitulo = UILabel()
        self.botaoEditar = UIButton()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.viewPrincipal.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.viewPrincipal.backgroundColor = .white
        
        self.iconeImageView.contentMode = .scaleAspectFill
        
        self.botaoFechar.setImage(UIImage(named: "fechar"), for: .normal)
        self.botaoFechar.addTarget(self, action: #selector(botaoFecharPressionado), for: .touchUpInside)
        
        self.iconeImageView.backgroundColor = .blue
        self.labelTitulo.text = "asndoiashdoashohdaoihdoaihioasdhioahdoiadhoaidhsoiahdoiahdoiashdoaianoasioasohasassauhisauasiuhas"
        
        self.iconeImageView.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.labelTitulo.font = UIFont.regular(size: 18)
        self.labelTitulo.numberOfLines = 0
        self.labelTitulo.textAlignment = .center
        
        
        self.botaoEditar.backgroundColor = SelecionadorDeCor.barraDeNavegacao.cor
        self.botaoEditar.setTitle("DELETAR", for: .normal)
        self.botaoEditar.setTitleColor(.white, for: .normal)
        self.botaoEditar.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        
        self.botaoEditar.addTarget(self, action: #selector(botaoEditarPressionado), for: .touchUpInside)

    }
    
    func buildViews() {
        self.viewPrincipal.addSubview(self.botaoFechar)
        self.viewPrincipal.addSubview(self.iconeImageView)
        self.viewPrincipal.addSubview(self.botaoEditar)
        self.viewPrincipal.addSubview(self.labelTitulo)
        self.addSubview(self.viewPrincipal)
        
    }
    
    func setupConstraints() {
        self.viewPrincipal.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(16))
            make.height.equalToSuperview().multipliedBy(0.7)
            make.centerY.equalToSuperview()
        }
        
        self.botaoFechar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.height.width.equalTo(AtributosDeLayout.margemPadrao * 2)
        }
        
        self.iconeImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(AtributosDeLayout.capturaValorResponsivo(200))
            make.centerX.equalToSuperview()
            make.top.equalTo(self.botaoFechar.snp.bottom)
        }
        
        self.labelTitulo.snp.makeConstraints { (make) in
            make.top.equalTo(self.iconeImageView.snp.bottom).offset(AtributosDeLayout.capturaValorResponsivo(16))
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(16))
        }
        
        self.botaoEditar.snp.makeConstraints { (make) in
            make.trailing.leading.bottom.equalToSuperview().inset(AtributosDeLayout.margemPadrao * 2)
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(40))
        }
    }
    
    @objc func botaoFecharPressionado(){
        self.responder?.botaoFecharPressionado()
    }
    
    @objc func botaoEditarPressionado(){
        self.responder?.botaoEditarPressionado()
    }
}
