//
//  FormularioSiglaView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import UIKit

class FormularioSiglaView: UIView, CodableView {
    
    var viewPrincipal: UIView
    var botaoFechar: UIButton
    var lbTitulo: UILabel
    var iconeImageView: UIImageView
    var entradaTexto: UITextField
    var viewLinha: UIView
    var botaoProsseguir: UIButton
    
    var responder: FormularioSiglaViewResponder?
    var texto: String = ""
    
    override init(frame: CGRect) {
        
        self.viewPrincipal = UIView()
        self.botaoFechar = UIButton()
        self.lbTitulo = UILabel()
        self.iconeImageView = UIImageView()
        self.entradaTexto = UITextField()
        self.viewLinha = UIView()
        self.botaoProsseguir = UIButton()
        
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.viewPrincipal.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.viewPrincipal.backgroundColor = .white
        
        self.viewLinha.backgroundColor = .gray
        
        self.lbTitulo.numberOfLines = 0
        self.lbTitulo.font = UIFont.medium(size: 20)
        self.lbTitulo.textColor = .black
        self.lbTitulo.textAlignment = .left
        self.lbTitulo.text = "Olá, é necessario definir um nome para esta turma, exemplo: 3ª ano B"
        
        self.iconeImageView.contentMode = .scaleAspectFit
        self.entradaTexto.textColor = .black
        self.entradaTexto.delegate = self
        
        self.botaoProsseguir.setTitleColor(.white, for: .normal)
        self.botaoProsseguir.backgroundColor = .gray
        self.botaoProsseguir.layer.cornerRadius = AtributosDeLayout.margemPadrao
        self.botaoProsseguir.setTitle("SALVAR", for: .normal)
        self.botaoProsseguir.addTarget(self, action: #selector(botaoProsseguirPressionado), for: .touchUpInside)
        
        self.botaoFechar.setImage(UIImage(named: "fechar"), for: .normal)
        self.botaoFechar.addTarget(self, action: #selector(botaoFecharPressionado), for: .touchUpInside)
        
        self.entradaTexto.autocorrectionType = .no
        self.entradaTexto.autocapitalizationType = .none
    }
    
    func buildViews() {
        self.addSubview(self.viewPrincipal)
        self.viewPrincipal.addSubview(self.lbTitulo)
        self.viewPrincipal.addSubview(self.botaoFechar)
        self.viewPrincipal.addSubview(self.botaoProsseguir)
        self.viewPrincipal.addSubview(self.iconeImageView)
        self.viewPrincipal.addSubview(self.entradaTexto)
        self.viewPrincipal.addSubview(self.viewLinha)
    }
    
    func setupConstraints() {
        self.viewPrincipal.snp.makeConstraints { (make) in
            make.top.equalTo(AtributosDeLayout.capturaValorResponsivo(UIScreen.main.bounds.height * 0.6))
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(AtributosDeLayout.capturaValorResponsivo(8))
        }
        
        self.botaoFechar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.height.width.equalTo(AtributosDeLayout.margemPadrao * 2)
        }
        
        self.viewLinha.snp.makeConstraints { (make) in
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(1))
            make.leading.equalTo(botaoProsseguir.snp.leading)
            make.trailing.equalTo(botaoProsseguir.snp.trailing)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(AtributosDeLayout.capturaValorResponsivo(16))
        }
        
        self.iconeImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(AtributosDeLayout.capturaValorResponsivo(24))
            make.leading.equalTo(viewLinha.snp.leading)
            make.bottom.equalTo(viewLinha.snp.top)
        }
        
        self.entradaTexto.snp.makeConstraints { (make) in
            make.leading.equalTo(iconeImageView.snp.trailing).offset(AtributosDeLayout.capturaValorResponsivo(8))
            make.trailing.equalTo(botaoProsseguir.snp.trailing)
            make.height.equalTo(iconeImageView.snp.height)
            make.bottom.equalTo(viewLinha.snp.bottom)
        }
        
        self.lbTitulo.snp.makeConstraints { (make) in
            make.top.equalTo(botaoFechar.snp.bottom).offset(AtributosDeLayout.capturaValorResponsivo(8))
            make.leading.equalTo(botaoProsseguir.snp.leading)
            make.trailing.equalTo(botaoProsseguir.snp.trailing)
        }
        
        self.botaoProsseguir.snp.makeConstraints { (make) in
            make.trailing.leading.bottom.equalToSuperview().inset(AtributosDeLayout.margemPadrao * 2)
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(50))
        }
    }
    
    func definirStatusDoBotao(estaAtivado: Bool){
        if estaAtivado {
            self.botaoProsseguir.backgroundColor = SelecionadorDeCor.botaoProsseguir.cor
            self.botaoProsseguir.isEnabled = true
        }else{
            self.botaoProsseguir.backgroundColor = .gray
            self.botaoProsseguir.isEnabled = false
        }
    }
    
    @objc func botaoProsseguirPressionado(){
        self.responder?.botaoProsseguirPressionado(texto: self.texto)
    }
    
    @objc func botaoFecharPressionado(){
        self.responder?.botaoFecharPressionado()
    }
}

//MARK: Manipulacao de Layout
extension FormularioSiglaView {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardHeight = keyboardSize.cgRectValue.height
        adjustTextFieldPosition(keyboardHeight: keyboardHeight)
    }
    
    @objc func keyboardWillHide() {
        self.viewPrincipal.snp.remakeConstraints { (remake) in
            remake.top.equalTo(AtributosDeLayout.capturaValorResponsivo(UIScreen.main.bounds.height * 0.6))
            remake.leading.trailing.equalToSuperview()
            remake.bottom.equalToSuperview().offset(AtributosDeLayout.capturaValorResponsivo(8))
        }
        UIView.animate(withDuration: 0.5) { self.layoutIfNeeded() }
    }
    
    func adjustTextFieldPosition(keyboardHeight: CGFloat) {
        self.viewPrincipal.snp.remakeConstraints { (remake) in
            remake.height.equalTo(AtributosDeLayout.capturaValorResponsivo(keyboardHeight + 16))
            remake.leading.trailing.equalToSuperview()
            remake.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(keyboardHeight - 8))
        }
        UIView.animate(withDuration: 0.5) { self.layoutIfNeeded() }
    }
    
    
    func hideKeyboard(_ completion: @escaping (()->()) = { }) {
        entradaTexto.resignFirstResponder()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: completion)
    }
    
}

extension FormularioSiglaView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let texto = textField.text else {
            return false
        }
        
        let textCompleto = "\(texto)\(string)"
        if textCompleto.count >= 5 {
            self.texto = textCompleto
            definirStatusDoBotao(estaAtivado: true)
        }else {
            definirStatusDoBotao(estaAtivado: false)
        }
        return true
    }
}
