//
//  FormularioSelecionaTipoExportacaoView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/01/21.
//

import UIKit

class FormularioSelecionaTipoExportacaoView: UIView, CodableView {
    
    var viewPrincipal: UIView
    var botaoFechar: UIButton
    var botaoProsseguir: UIButton
    var labelTitulo: UILabel
    var preVisualizacao: BoletimInicialView
    var labelDescricao: UILabel
    var collectionView: UICollectionView
    var responder: FormularioSelecionaTipoExportacaoViewResponder?
    var tipoExportacao: String?
    
    override init(frame: CGRect) {
        self.viewPrincipal = UIView()
        self.botaoFechar = UIButton()
        self.botaoProsseguir = UIButton()
        self.labelTitulo = UILabel()
        self.preVisualizacao = BoletimInicialView(frame: frame)//ImagemZoomView(frame: frame)
        self.labelDescricao = UILabel()
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.viewPrincipal.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.viewPrincipal.backgroundColor = .white
        
        self.botaoFechar.setImage(UIImage(named: "fechar"), for: .normal)
        self.botaoFechar.addTarget(self, action: #selector(botaoFecharPressionado), for: .touchUpInside)
    
        self.botaoProsseguir.setTitle("PROSSEGUIR", for: .normal)
        self.botaoProsseguir.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.botaoProsseguir.setTitleColor(.black, for: .normal)
        self.botaoProsseguir.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        self.botaoProsseguir.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.botaoProsseguir.isEnabled = false
        self.botaoProsseguir.addTarget(self, action: #selector(botaoProsseguirPressionado), for: .touchUpInside)
        
        self.labelTitulo.text = "Pré visualização"
        self.labelTitulo.textColor = .black
        self.labelTitulo.font = UIFont.boldSystemFont(ofSize: 22)
        

        self.preVisualizacao.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        
        self.labelDescricao.text = "Selecione o formato que deseja exportar"
        self.labelDescricao.textColor = .black
        self.labelDescricao.font = UIFont.regular(size: 12)
        self.labelDescricao.numberOfLines = 0
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = 0.0
        
        self.collectionView.register(SelecaoTipoExportacaoCollectionViewCell.self, forCellWithReuseIdentifier: SelecaoTipoExportacaoCollectionViewCell.identificador)
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.bounces = false
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.alwaysBounceVertical = true
        self.collectionView.backgroundColor = UIColor.clear
    }
    
    func buildViews() {
        self.viewPrincipal.addSubview(self.botaoFechar)
        self.viewPrincipal.addSubview(self.botaoProsseguir)
        self.viewPrincipal.addSubview(self.labelTitulo)
        self.viewPrincipal.addSubview(self.preVisualizacao)
        self.viewPrincipal.addSubview(self.labelDescricao)
        self.viewPrincipal.addSubview(self.collectionView)
        self.addSubview(self.viewPrincipal)
    }
    
    func setupConstraints() {
        self.viewPrincipal.snp.makeConstraints { (make) in
            make.height.equalToSuperview().multipliedBy(0.86)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        self.botaoFechar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.height.width.equalTo(AtributosDeLayout.margemPadrao * 2)
        }
        
        self.labelTitulo.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.top.equalTo(self.botaoFechar.snp.bottom)
        }
        
        self.botaoProsseguir.snp.makeConstraints { (make) in
            make.height.equalTo(AtributosDeLayout.capturaValorResponsivo(50))
            make.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(40))
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(12))
        }
        
        self.preVisualizacao.snp.makeConstraints { (make) in
            make.top.equalTo(labelTitulo.snp.bottom).offset(AtributosDeLayout.margemPadrao)
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.height.equalToSuperview().multipliedBy(AtributosDeLayout.capturaValorResponsivo(0.58))
        }
        
        self.labelDescricao.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.top.equalTo(self.preVisualizacao.snp.bottom).offset(AtributosDeLayout.capturaValorResponsivo(AtributosDeLayout.margemPadrao))
        }
        
        self.collectionView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.margemPadrao)
            make.top.equalTo(self.labelDescricao.snp.bottom).offset(AtributosDeLayout.capturaValorResponsivo(8))
            make.height.equalToSuperview().multipliedBy(AtributosDeLayout.capturaValorResponsivo(0.05))
        }
    }
    
    @objc func botaoFecharPressionado(){
        self.responder?.botaoFecharPressionado()
    }
    
    @objc func botaoProsseguirPressionado(){
        guard let tipoExportacao = self.tipoExportacao else {
            return
        }
        self.responder?.botaoProsseguirPressionado(tipoExportacao: tipoExportacao)
    }
    
    func definirStatusBotaoAoSelecionarTipoExportacao(tipoExportacao: String){
        self.tipoExportacao = tipoExportacao
        self.botaoProsseguir.backgroundColor = SelecionadorDeCor.botaoProsseguir.cor
        self.botaoProsseguir.isEnabled = true
    }
    
    func populaView(disciplinas: [Disciplina]){
       // let boletimInicialView = BoletimInicialView()
       // boletimInicialView.disciplinas = disciplinas
        //let imgboletimInicialView = boletimInicialView.comoImagem()
        //self.preVisualizacao.populaView(imagem: imgboletimInicialView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
    }
}
