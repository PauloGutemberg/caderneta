//
//  BotaoExportarBoletimView.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/01/21.
//

import UIKit

class BotaoExportarBoletimView: UIView, CodableView {
    
    var botaoExportar: UIButton
    var responder: BotaoExportarBoletimViewResponder?
    
    override init(frame: CGRect) {
        self.botaoExportar = UIButton()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        
        self.botaoExportar.backgroundColor = SelecionadorDeCor.barraDeNavegacao.cor
        self.botaoExportar.setTitle("Compartilhar Boletim", for: .normal)
        self.botaoExportar.setTitleColor(.white, for: .normal)
        self.botaoExportar.layer.cornerRadius = AtributosDeLayout.capturaValorResponsivo(8)
        self.botaoExportar.layer.borderWidth = AtributosDeLayout.capturaValorResponsivo(1)
        
        self.botaoExportar.addTarget(self, action: #selector(botaoExportarPressionado), for: .touchUpInside)
    }
    
    func buildViews() {
        self.addSubview(self.botaoExportar)
    }
    
    func setupConstraints() {
        self.botaoExportar.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(20))
            make.bottom.equalToSuperview().inset(AtributosDeLayout.capturaValorResponsivo(8))
            make.height.equalToSuperview().multipliedBy(0.6)
        }
    }
    
    @objc func botaoExportarPressionado(){
        self.responder?.botaoExportarBoletimPressionado()
    }
}
