//
//  AdicionarEscolaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 12/12/20.
//

import UIKit

class FormularioEscolaViewController: BaseViewController<FormularioEscolaView> {

    var parametros: FormularioEscolaViewControllerModel?
    var presenter: FormularioEscolaPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioEscolaPresenter(output: self)
        configuraTitulo()
    }
    
    func configuraTitulo(){
        guard let parametros = self.parametros else { return }
        if parametros.estaEditandoDados {
            titulo = "Editar Escola"
        }else{
            titulo = "Adicionar escola"
        }
    }
}

extension FormularioEscolaViewController: FormularioEscolaViewResponder {
    func botaoProsseguirPressionado(texto: String) {
        let escola = Escola(nome: texto, caminhoImagem: nil, id: UUID().uuidString, turmas: Array<String>())
        self.presenter?.inserirEscola(escola: escola)
        self.dismiss(animated: true){
            self.parametros?.formularioEscolaViewControllerResponder?.botaoProsseguirPressionado()
        }
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioEscolaViewController: FormularioEscolaOutput {
    
}
