//
//  FormularioNomeViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 01/01/21.
//

import UIKit

class FormularioNomeViewController: BaseViewController<FormularioNomeView> {
    
    var parametros: FormularioNomeViewControllerModel?
    var presenter: FormularioNomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioNomePresenter(output: self)
        populaView()
    }
    
    func populaView(){
        guard let parametros = self.parametros else { return }
        self.viewAssociada.populaView(parametros: parametros)
    }
    
}

extension FormularioNomeViewController: FormularioNomeViewResponder {
    func botaoProsseguirPressionado(texto: String) {
        guard let parametros = self.parametros else { return }
        let aluno = Aluno(nome: texto, dataNascimento: parametros.dataNascimento, genero: parametros.genero.descricao, caminhoImagem: nil, id: UUID().uuidString)
        self.presenter?.inserirAluno(aluno: aluno)
        parametros.turma.alunos.append(aluno.id)
        for idDisciplina in parametros.turma.disciplinas {
            let participacao = ParticipacaoNaTurma(id: UUID().uuidString, idDisciplina: idDisciplina, idAluno: aluno.id, idTurma: parametros.turma.id, primeiroPeriodo: [], segundoPeriodo: [], terceiroPeriodo: [], quartoPeriodo: [], recuperacao: [])
            self.presenter?.adicionarParticipacaoAlunoNaTurma(participacao: participacao)
            parametros.turma.participacoesNaTurma.append(participacao.id)
            
        }
        self.presenter?.atualizaTurmaComNovoAluno(turma: parametros.turma)
        self.dismiss(animated: true) {
            parametros.formularioNomeViewControllerResponder?.botaoProsseguirAoEscreverUmNomePressionado()
        }
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioNomeViewController: FormularioNomeOutput{
    
}
