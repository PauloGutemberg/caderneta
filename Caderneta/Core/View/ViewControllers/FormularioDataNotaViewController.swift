//
//  FormularioDataNotaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import UIKit

class FormularioDataNotaViewController: BaseViewController<FormularioDataNotaView> {
    
    var parametros: FormularioDataNotaViewControllerModel?
    var presenter: FormularioDataNotaPresenter?
    
    var data: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioDataNotaPresenter(output: self)
    }
    
    func atualizarParticipacao(participacao: ParticipacaoNaTurma, nota: Nota){
        guard let parametros = self.parametros else { return }
        switch parametros.periodo.descricao {
        case PeriodoDescricao.primeiro.rawValue:
            participacao.primeiroPeriodo.append(nota.id)
        case PeriodoDescricao.segundo.rawValue:
            participacao.segundoPeriodo.append(nota.id)
        case PeriodoDescricao.terceiro.rawValue:
            participacao.terceiroPeriodo.append(nota.id)
        case PeriodoDescricao.quarto.rawValue:
            participacao.quartoPeriodo.append(nota.id)
        case PeriodoDescricao.recuperacao.rawValue:
            participacao.recuperacao.append(nota.id)
        default:
            print("Nada há fazer por enquanto")
        }
        self.presenter?.atualizarParticipacao(participacao: participacao)
    }
    
}

extension FormularioDataNotaViewController: FormularioSelecionaDataViewResponder {
    func botaoProsseguirPressionado(data: String) {
        guard let parametros = self.parametros else { return }
        self.data = data
        self.presenter?.obterParticipacao(idTurma: parametros.turma.id, idDisciplina: parametros.disciplina.id, idAluno: parametros.aluno.id)
        self.dismiss(animated: true){
            self.parametros?.formularioDataNotaViewControllerResponder?.botaoProsseguirAoSelecionarDataPressionado()
        }
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension FormularioDataNotaViewController: FormularioDataNotaOutput {
    func obterParticipacao(participacao: ParticipacaoNaTurma) {
        guard let parametros = self.parametros else { return }
        let nota = Nota(valor: parametros.nota, observacao: parametros.descricao, dataNota: self.data, titulo: parametros.titulo, colapso: false, id: UUID().uuidString, seguimento: "\(parametros.tempoNota.id)", periodo: "\(parametros.periodo.id)")
        self.presenter?.inserirNota(nota: nota)
        atualizarParticipacao(participacao: participacao, nota: nota)
    }
}
