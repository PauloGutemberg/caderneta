//
//  FormularioSelecionaGeneroViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 28/12/20.
//

import UIKit

class FormularioSelecionaGeneroViewController: BaseViewController<FormularioSelecionaGeneroView> {
    
    var parametros: FormularioSelecionaGeneroViewControllerModel?
    var presenter: FormularioSelecionaGeneroPresenter?
    var generos: [Genero] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.viewAssociada.collectionView.dataSource = self
        self.viewAssociada.collectionView.delegate = self
        self.presenter = FormularioSelecionaGeneroPresenter(output: self)
        self.presenter?.recuperaGeneros()
    }
    
    func abreFormularioSelecionaData(genero: Genero){
        guard let parametros = self.parametros else {
            return
        }
        let formularioSelecionaDataViewControllerModel: FormularioSelecionaDataViewControllerModel = FormularioSelecionaDataViewControllerModel(genero: genero, formularioSelecionaDataViewControllerResponder: self, turma: parametros.turma)
        self.mainCoordinator?.abreFormularioSelecionaData(formularioSelecionaDataViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionaGeneroViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.generos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelecaoImagemCollectionViewCell.identificador, for: indexPath) as? SelecaoImagemCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.populaCelula(genero: self.generos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.viewAssociada.definirStatusBotaoAoSelecionarGenero(genero: self.generos[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let espacoBordas = collectionView.frame.width * 0.05
        return UIEdgeInsets(top: 0, left: espacoBordas, bottom: 0, right: espacoBordas)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2.6, height: UIScreen.main.bounds.width * 0.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension FormularioSelecionaGeneroViewController: FormularioSelecionaGeneroViewResponder {
    func botaoProsseguirPressionado(genero: Genero) {
        print(genero.descricao)
        abreFormularioSelecionaData(genero: genero)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionaGeneroViewController: FormularioSelecionaGeneroOutput {
    func retornaGeneros(generos: [Genero]) {
        self.generos = generos
        self.viewAssociada.collectionView.reloadData()
    }
}

extension FormularioSelecionaGeneroViewController: FormularioSelecionaDataViewControllerResponder {
    
    func botaoProsseguirAoSelecionarDataPressionado(){
        guard let parametros = self.parametros else { return }
        self.dismiss(animated: true) {
            parametros.formularioSelecionaGeneroViewControllerResponder?.botaoProsseguirAoSelecionarGeneroPressionado()
        }
    }
}
