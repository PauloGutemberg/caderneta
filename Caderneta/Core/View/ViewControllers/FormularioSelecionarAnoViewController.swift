//
//  FormularioSelecionarAnoViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/12/20.
//

import UIKit

class FormularioSelecionarAnoViewController: BaseViewController<FormularioSelecionarAnoView> {

    var parametros: FormularioSelecionarAnoViewControllerModel?
    var presenter: FormularioSelecionarAnoPresenter?
    var anos: [Ano] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioSelecionarAnoPresenter(output: self)
        self.presenter?.recuperarAnos()
        configuraTableView()
    }
    
    func configuraTableView(){
        self.viewAssociada.tableView.register(SelecaoDisciplinaTableViewCell.self, forCellReuseIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.separatorStyle = .none
    }
    
    func definirAnosComoNaoSelecionado(){
        for ano in self.anos {
            ano.estaSelecionado = false
        }
    }
    
    func abrirTelaSelecionarTurno(ano: Ano) {
        guard let parametros = self.parametros else { return }
        let formularioSelecionarTurnoViewControllerModel: FormularioSelecionarTurnoViewControllerModel = FormularioSelecionarTurnoViewControllerModel(disciplinas: parametros.disciplinas, ano: ano, formularioSelecionarTurnoViewControllerResponder: self, escola: parametros.escola)
        self.mainCoordinator?.abreFormularioSelecionaTurno(formularioSelecionarTurnoViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionarAnoViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.anos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel, for: indexPath) as? SelecaoDisciplinaTableViewCell {
            cell.populaCelula(ano: anos[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        definirAnosComoNaoSelecionado()
        self.anos[indexPath.row].estaSelecionado = true
        if let anoSelecionado = self.anos.anoSelecionado(), anoSelecionado.estaSelecionado {
            self.viewAssociada.definirStatusBotaoAoSelecionarUmAno()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
}

extension FormularioSelecionarAnoViewController: FormularioSelecionarAnoOutput {
    func retornarAnos(anos: [Ano]) {
        self.anos = anos
        self.viewAssociada.tableView.reloadData()
    }
}

extension FormularioSelecionarAnoViewController: FormularioSelecionarAnoViewResponder {
    func botaoProsseguirPressionado() {
        guard let anoSelecionado = self.anos.anoSelecionado() else { return }
        abrirTelaSelecionarTurno(ano: anoSelecionado)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionarAnoViewController: FormularioSelecionarTurnoViewControllerResponder {
    func botaoProsseguirAoSelecionarTurnoPressionado() {
        guard let parametros = self.parametros else { return }
        self.dismiss(animated: true) {
            parametros.formularioSelecionarAnoViewControllerResponder?.botaoProsseguirAoSelecionarAnoPressionado()
        }
    }
}
