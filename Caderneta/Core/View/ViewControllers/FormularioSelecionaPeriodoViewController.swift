//
//  FormularioSelecionaPeriodoViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 06/01/21.
//

import UIKit

class FormularioSelecionaPeriodoViewController: BaseViewController<FormularioSelecionaPeriodoView> {
    
    var parametros: FormularioSelecionaPeriodoViewControllerModel?
    var presenter: FormularioSelecionaPeriodoPresenter?
    var periodos: [Periodo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioSelecionaPeriodoPresenter(output: self)
        self.presenter?.recuperarPeriodos()
        configuraTableView()
    }
    
    func configuraTableView(){
        self.viewAssociada.tableView.register(SelecaoDisciplinaTableViewCell.self, forCellReuseIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.separatorStyle = .none
    }
    
    func definirPeriodosComoNaoSelecionado(){
        for periodo in self.periodos {
            periodo.estaSelecionado = false
        }
    }
}

extension FormularioSelecionaPeriodoViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.periodos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel, for: indexPath) as? SelecaoDisciplinaTableViewCell {
            cell.populaCelula(periodo: self.periodos[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        definirPeriodosComoNaoSelecionado()
        self.periodos[indexPath.row].estaSelecionado = true
        if let periodoSelecionado = self.periodos.periodoSelecionado(), periodoSelecionado.estaSelecionado {
            self.viewAssociada.definirStatusBotaoAoSelecionarUmPeriodo()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
}


extension FormularioSelecionaPeriodoViewController: FormularioSelecionaPeriodoViewResponder {
    func botaoProsseguirPressionado() {
        guard let periodoSelecionado = self.periodos.periodoSelecionado() else { return }
        guard let parametros = self.parametros else { return }
        
        let formularioTituloNotaViewControllerModel: FormularioTituloNotaViewControllerModel = FormularioTituloNotaViewControllerModel(periodo: periodoSelecionado, formularioTituloNotaViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
        self.mainCoordinator?.abreFormularioTituloNota(formularioTituloNotaViewControllerModel, chamador: self)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionaPeriodoViewController: FormularioSelecionaPeriodoOutput {
    func retornaPeriodos(periodos: [Periodo]) {
        self.periodos = periodos
        self.viewAssociada.tableView.reloadData()
    }
}

extension FormularioSelecionaPeriodoViewController: FormularioTituloNotaViewControllerResponder {
    func botaoProsseguirAoEscreverUmTituloPressionado() {
        self.dismiss(animated: true) {
            self.parametros?.formularioSelecionaPeriodoViewControllerResponder?.botaoProsseguirAoSelecionarPeriodoPressionado()
        }
    }
}
