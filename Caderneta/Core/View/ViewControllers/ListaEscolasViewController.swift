//
//  ListaEscolasViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/12/20.
//

import UIKit

class ListaEscolasViewController: BaseViewController<ListaEscolasView> {
    
    var presenter: ListaEscolasPresenter?
    var escolas: [Escola] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.tableView.register(ListaTableViewCell.self, forCellReuseIdentifier: ListaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.responder = self
        self.presenter = ListaEscolasPresenter(output: self)
        self.presenter?.recuperarEscolas()
    }
}

extension ListaEscolasViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return escolas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.viewAssociada.tableView.dequeueReusableCell(withIdentifier: ListaTableViewCell.identificadorReusavel, for: indexPath) as? ListaTableViewCell else {
            return UITableViewCell()
        }
        cell.populaCelula(escola: self.escolas[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let escola = self.escolas[seguro: indexPath.row] else {
            return
        }
        self.mainCoordinator?.abreListaDeSalasDeAula(ListaSalasDeAulaViewControllerModel(escola: escola))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.width * 76/360
    }
}

extension ListaEscolasViewController: ListaViewResponder {
    func adicionarPressionado() {
      var formularioEscolaViewControllerModel: FormularioEscolaViewControllerModel = FormularioEscolaViewControllerModel()
        formularioEscolaViewControllerModel.formularioEscolaViewControllerResponder = self
        self.mainCoordinator?.abreFormularioAdicionarEscola(formularioEscolaViewControllerModel, chamador: self)
        //mainCoordinator?.abreTelaEditDele(EditDeleViewControllerModel(), chamador: self)
    }
}

extension ListaEscolasViewController: FormularioEscolaViewControllerResponder {
    func botaoProsseguirPressionado() {
        self.presenter?.recuperarEscolas()
    }
}

extension ListaEscolasViewController: TabBarViewControllerResponder {
    
}

extension ListaEscolasViewController: ListaEscolasOutput {
    func retornaEscolas(escolas: [Escola]) {
        self.escolas = escolas
        self.viewAssociada.tableView.reloadData()
    }
}
