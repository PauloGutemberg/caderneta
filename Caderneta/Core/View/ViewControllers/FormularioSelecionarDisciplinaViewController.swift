//
//  FormularioSelecionarDisciplinaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/12/20.
//

import UIKit

class FormularioSelecionarDisciplinaViewController: BaseViewController<FormularioSelecionarDisciplinaView> {

    var parametros: FormularioSelecionarDisciplinaViewControllerModel?
    var presenter: FormularioSelecionarDisciplinaPresenter?
    var disciplinas: [Disciplina] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioSelecionarDisciplinaPresenter(output: self)
        self.presenter?.recuperaDisciplinas()
        configuraTableView()
    }
    
    func configuraTableView(){
        self.viewAssociada.tableView.register(SelecaoDisciplinaTableViewCell.self, forCellReuseIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.separatorStyle = .none
    }
    
    func abreTelaSelecionarAno(disciplinasSelecionadas: [Disciplina]){
        guard let parametros = self.parametros else {
            return
        }
        
        var formularioSelecionarAnoViewControllerModel: FormularioSelecionarAnoViewControllerModel = FormularioSelecionarAnoViewControllerModel(disciplinas: disciplinasSelecionadas, escola: parametros.escola)
        formularioSelecionarAnoViewControllerModel.formularioSelecionarAnoViewControllerResponder = self
        self.mainCoordinator?.abreFormularioSelecionarAno(formularioSelecionarAnoViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionarDisciplinaViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.disciplinas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel, for: indexPath) as? SelecaoDisciplinaTableViewCell {
            cell.populaCelula(disciplina: self.disciplinas[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.disciplinas[indexPath.row].estaSelecionada = true
        if !(self.disciplinas.disciplinasSelecionadas()?.isEmpty ?? true){
            self.viewAssociada.definirStatusBotaoAoSelecionarAoMenosUmaDisciplina()
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.disciplinas[indexPath.row].estaSelecionada = false
        if self.disciplinas.disciplinasSelecionadas()?.isEmpty ?? true {
            self.viewAssociada.definirStatusBotaoAoRemoverTodasAsSelecoesDeDisciplina()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
}

extension FormularioSelecionarDisciplinaViewController: FormularioSelecionarDisciplinaViewResponder {
    func botaoProsseguirPressionado() {
        var disciplinasSelecionadas: [Disciplina] = []
        self.disciplinas.disciplinasSelecionadas()?.forEach({ (disciplinaAuxiliar) in
            disciplinasSelecionadas.append(disciplinaAuxiliar)
        })
        abreTelaSelecionarAno(disciplinasSelecionadas: disciplinasSelecionadas)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionarDisciplinaViewController: FormularioSelecionarDisciplinaOutput {
    func retornaDisciplinas(disciplinas: [Disciplina]) {
        self.disciplinas = disciplinas
        self.viewAssociada.tableView.reloadData()
    }
}

extension FormularioSelecionarDisciplinaViewController: FormularioSelecionarAnoViewControllerResponder {
    func botaoProsseguirAoSelecionarAnoPressionado() {
        self.dismiss(animated: true){
            self.parametros?.formularioSelecionarDisciplinaViewControllerResponder?.botaoProsseguirAoSelecionarDisciplinaPressionado()
        }
    }
}
