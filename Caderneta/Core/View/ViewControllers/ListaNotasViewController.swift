//
//  ListaNotasViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import UIKit

class ListaNotasViewController: BaseViewController<ListaNotasView> {
    
    var tipoCelula: TipoCelulaListaNotas?
    
    var parametros: ListaNotasViewControllerModel?
    var cabecalhoDisciplinas: [Disciplina] = []
    var disciplinaAbaSelecionada: Disciplina?
    
    var dadosPaginaAtualListaNotas = [Nota]()
    var dadosResultadoFinalListaNotas = [ResultadoFinal]()
    var presenter: ListaNotasPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = ListaNotasPresenter(output: self)
        recuperarDisciplinas()
        criaPaginasParaListaNotas()
        configuraComponentes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.titulo = "Notas"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexResultadoFinal = self.cabecalhoDisciplinas.firstIndex(where: { (disciplina) -> Bool in
            disciplina.nome == "RESULT. FINAL"
        }){
            let indexPenultimaAba = indexResultadoFinal - 1
            guard let _ = self.cabecalhoDisciplinas[seguro: indexPenultimaAba] else { return }
            self.viewAssociada.collectionView.selectItem(at: IndexPath(item: indexPenultimaAba, section: 0), animated: true, scrollPosition: .centeredHorizontally)
            guard let disciplina = self.cabecalhoDisciplinas[seguro: indexPenultimaAba] else { return }
            processaAbaSelecionada(tituloAba: disciplina.nome)
            self.disciplinaAbaSelecionada = disciplina
            viewAssociada.scrollToPage(page: indexPenultimaAba)
        }
    }
    
    func configuraComponentes() {
        self.viewAssociada.responder = self
        self.viewAssociada.collectionView.dataSource = self
        self.viewAssociada.collectionView.delegate = self
        self.viewAssociada.paginasDisciplinas.forEach { (pagina) in
            pagina.tableView.delegate = self
            pagina.tableView.dataSource = self
            pagina.tableView.register(DetalhamentoListaNotasCell.self, forCellReuseIdentifier: DetalhamentoListaNotasCell.identificador)
            pagina.tableView.register(ResultadoFinalListaNotasCell.self, forCellReuseIdentifier: ResultadoFinalListaNotasCell.identificador)
            pagina.tableView.separatorStyle = .none
            pagina.tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    func trataCelulaResultadoFinal(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: ResultadoFinalListaNotasCell.identificador, for: indexPath) as? ResultadoFinalListaNotasCell else {
            return UITableViewCell()
        }
        cell.populaCelula(disciplina: dadosResultadoFinalListaNotas[indexPath.row].disciplina, nota: dadosResultadoFinalListaNotas[indexPath.row].nota)
        return cell
    }
    
    func trataCelulaDetalhamento(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: DetalhamentoListaNotasCell.identificador, for: indexPath) as? DetalhamentoListaNotasCell else {
            return UITableViewCell()
        }
        cell.populaCelula(nota: dadosPaginaAtualListaNotas[indexPath.row])
        return cell
    }
    
    func processaAbaSelecionada(tituloAba: String){
        
        guard let tableView = self.viewAssociada.retornaTableviewPaginaDisciplina(chaveAssociada: tituloAba) else { return }
        guard let parametros = self.parametros else { return }
        guard let disciplina = self.disciplinaAbaSelecionada else { return }
        self.dadosPaginaAtualListaNotas.removeAll()
        tableView.reloadData()
        tableView.backgroundView = UIView()
        if tituloAba == "RESULT. FINAL" {
            self.dadosResultadoFinalListaNotas = []
            for disciplinaAuxiliar in self.cabecalhoDisciplinas {
                self.presenter?.recuperarNotas(alunoID: parametros.aluno.id, turmaID: parametros.turma.id, disciplinaID: disciplinaAuxiliar.id, chavePaginaNotasAssociada: tituloAba)
            }
        }else{
            self.presenter?.recuperarNotas(alunoID: parametros.aluno.id, turmaID: parametros.turma.id, disciplinaID: disciplina.id, chavePaginaNotasAssociada: tituloAba)
        }
    }
    
    func criaPaginasParaListaNotas(){
        self.cabecalhoDisciplinas.forEach { (disciplina) in
            self.viewAssociada.criaPaginaDisciplina(chaveAssociada: disciplina.nome)
        }
    }
    
    func recuperarDisciplinas(){
        guard let parametros = self.parametros else { return }
        self.presenter?.recuperarDisciplinas(ids: parametros.turma.disciplinas)
    }
}

extension ListaNotasViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tipoCelula = self.tipoCelula else {
            return 0
        }
        switch tipoCelula {
        case .detalhamento:
            return self.dadosPaginaAtualListaNotas.count
        case .resultadoFinal:
            return self.dadosResultadoFinalListaNotas.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tipoCelula = self.tipoCelula else {
            return UITableViewCell()
        }
        switch tipoCelula {
        case .detalhamento:
            return trataCelulaDetalhamento(tableView, indexPath: indexPath)
        case .resultadoFinal:
            return trataCelulaResultadoFinal(tableView, indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((tableView.cellForRow(at: indexPath) as? DetalhamentoListaNotasCell) != nil) {
            let nota = self.dadosPaginaAtualListaNotas[indexPath.row]
            nota.colapso = !nota.colapso
            self.dadosPaginaAtualListaNotas[indexPath.row] = nota
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  ((tableView.cellForRow(at: indexPath) as? DetalhamentoListaNotasCell) != nil) {
            if self.dadosPaginaAtualListaNotas[indexPath.row].colapso {
                return AtributosDeLayout.capturaValorResponsivo(130)
            }else{
                return AtributosDeLayout.capturaValorResponsivo(60)
            }
        }
        
        if  ((tableView.cellForRow(at: indexPath) as? ResultadoFinalListaNotasCell) != nil) {
            return AtributosDeLayout.capturaValorResponsivo(80)
        }
        
        return AtributosDeLayout.capturaValorResponsivo(60)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(60)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewLimpa: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        viewLimpa.backgroundColor = .clear
        guard let tipoCelula = self.tipoCelula else {
            return viewLimpa
        }
        switch tipoCelula {
        case .detalhamento:
            return viewLimpa
        case .resultadoFinal:
            let exportarBoletimView = BotaoExportarBoletimView()
            exportarBoletimView.responder = self
            return exportarBoletimView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let tipoCelula = self.tipoCelula else {
            return 0
        }
        switch tipoCelula {
        case .detalhamento:
            return 0
        case .resultadoFinal:
            return AtributosDeLayout.capturaValorResponsivo(60)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewLimpa: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        viewLimpa.backgroundColor = .clear
        guard let tipoCelula = self.tipoCelula else {
            return viewLimpa
        }
        
        switch tipoCelula {
        case .detalhamento:
            let botaoAdicionarNotaView = BotaoAdicionarNotaView()
            botaoAdicionarNotaView.responder = self
            return botaoAdicionarNotaView
        case .resultadoFinal:
            let viewResultadoFinal = ResultadoFinalCabecalhoView()
            viewResultadoFinal.populaView(mensagem: "Em andamento, periodo letivo ainda não acabou, os resultados aqui apresentados são parciais.") //aprovador ou reprovaod
            return viewResultadoFinal
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let tipoCelula = self.tipoCelula else {
            return 0
        }
        switch tipoCelula {
        case .detalhamento:
            return AtributosDeLayout.capturaValorResponsivo(60)
        case .resultadoFinal:
            return AtributosDeLayout.capturaValorResponsivo(90)
        }
    }
}


extension ListaNotasViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cabecalhoDisciplinas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ListaNotasCabecalhoViewCell.identificador, for: indexPath) as? ListaNotasCabecalhoViewCell else {
            return UICollectionViewCell()
        }
        cell.configuraCelula(disciplina: self.cabecalhoDisciplinas[indexPath.row].nome, indice: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.viewAssociada.scrollToPage(page: indexPath.row)
        self.disciplinaAbaSelecionada = self.cabecalhoDisciplinas[seguro: indexPath.row]
        processaAbaSelecionada(tituloAba: self.cabecalhoDisciplinas[seguro: indexPath.row]?.nome ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let espacoBordas = collectionView.frame.width * 0.05
        return UIEdgeInsets(top: 0, left: espacoBordas, bottom: 0, right: espacoBordas)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2.3, height: UIScreen.main.bounds.width * 0.075)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension ListaNotasViewController: ListaNotasViewResponder {
    func abaSelecionada(aba: Int) {
        self.viewAssociada.collectionView.selectItem(at: IndexPath(item: aba, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        self.processaAbaSelecionada(tituloAba: self.cabecalhoDisciplinas[aba].nome)
    }
}

extension ListaNotasViewController: ListaNotasOutput {
    
    func retornaDisciplinas(disciplinas: [Disciplina]) {
        self.cabecalhoDisciplinas = disciplinas
        self.viewAssociada.collectionView.reloadData()
    }
    
    func retornaNotas(notas: [Nota], chavePaginaNotasAssociada: String) {
        self.dadosPaginaAtualListaNotas = notas
        self.tipoCelula = .detalhamento
        if let tableViewPaginaNotas = self.viewAssociada.retornaTableviewPaginaDisciplina(chaveAssociada: chavePaginaNotasAssociada) {
            tableViewPaginaNotas.reloadData()
        }
    }
    
    func retornaResultadoFinal(resultado: ResultadoFinal, chavePaginaNotasAssociada: String){
        self.dadosResultadoFinalListaNotas.append(resultado)
        self.tipoCelula = .resultadoFinal
        if let tableViewPaginaNotas = self.viewAssociada.retornaTableviewPaginaDisciplina(chaveAssociada: chavePaginaNotasAssociada) {
            tableViewPaginaNotas.reloadData()
        }
    }
}

extension ListaNotasViewController: BotaoAdicionarNotaViewResponder {
    func botaoAdicionarNotaPressionado() {
        guard let disciplina = self.disciplinaAbaSelecionada else { return }
        guard let parametros = self.parametros else { return }
        
        let formularioSelecionaPeriodoViewControllerModel: FormularioSelecionaPeriodoViewControllerModel = FormularioSelecionaPeriodoViewControllerModel(formularioSelecionaPeriodoViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: disciplina)
        self.mainCoordinator?.abreFormularioSelecionaPeriodo(formularioSelecionaPeriodoViewControllerModel, chamador: self)
    }
}

extension ListaNotasViewController: BotaoExportarBoletimViewResponder {
    func botaoExportarBoletimPressionado() {
        let formularioSelecionaTipoExportacaoViewControllerModel: FormularioSelecionaTipoExportacaoViewControllerModel = FormularioSelecionaTipoExportacaoViewControllerModel()
        self.mainCoordinator?.abreFormularioSelecionaTipoExportacao(formularioSelecionaTipoExportacaoViewControllerModel, chamador: self)
    }
}

extension ListaNotasViewController: FormularioSelecionaTipoExportacaoViewControllerResponder {
    
    func botaoProsseguirAoSelecionarTipoExportacaoPressionado() {
        print("foi exportado")
    }
}

extension ListaNotasViewController: FormularioSelecionaPeriodoViewControllerResponder {
    func botaoProsseguirAoSelecionarPeriodoPressionado() {
        guard let parametros = self.parametros else { return }
        guard let disciplina = self.disciplinaAbaSelecionada else { return }
        self.presenter?.recuperarNotas(alunoID: parametros.aluno.id, turmaID: parametros.turma.id, disciplinaID: disciplina.id, chavePaginaNotasAssociada: disciplina.nome)
    }
}
