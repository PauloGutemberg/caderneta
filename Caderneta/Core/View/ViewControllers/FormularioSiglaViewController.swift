//
//  FormularioSiglaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import UIKit

class FormularioSiglaViewController: BaseViewController<FormularioSiglaView> {
    
    var parametros: FormularioSiglaViewControllerModel?
    var presenter: FormularioSiglaPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = FormularioSiglaPresenter(output: self)
        self.viewAssociada.responder = self
    }
}

extension FormularioSiglaViewController: FormularioSiglaViewResponder {
    func botaoProsseguirPressionado(texto: String) {
        guard let parametros = self.parametros else { return }
        let turma: Turma = Turma(sigla: texto, ano: parametros.ano, turno: parametros.turno, disciplinas: parametros.disciplinas.Ids(), caminhoImagem: nil, id: UUID().uuidString, participacoesNaTurma: Array<String>(), alunos: Array<String>())
        self.presenter?.inserirTurma(turma: turma)
        parametros.escola.turmas.append(turma.id)
        self.presenter?.atualizaEscolaComNovaTurma(escola: parametros.escola)
        self.dismiss(animated: true) {
            parametros.formularioSiglaViewControllerResponder?.botaoProsseguirAoEscreverUmaSiglaPressionado()
        }
        
    }
   
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSiglaViewController: FormularioSiglaOutput {
    
}
