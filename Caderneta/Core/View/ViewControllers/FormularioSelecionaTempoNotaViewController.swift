//
//  FormularioSelecionaTempoNotaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import UIKit

class FormularioSelecionaTempoNotaViewController: BaseViewController<FormularioSelecionaTempoNotaView>{
    
    var parametros: FormularioSelecionaTempoNotaViewControllerModel?
    var presenter: FormularioSelecionaTempoNotaPresenter?
    var tempoNotas: [TempoNota] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        definirComponentes()
        configuraTableView()
    }
    
    func definirComponentes(){
        guard let parametros = self.parametros else { return }
        self.presenter = FormularioSelecionaTempoNotaPresenter(output: self, periodo: parametros.periodo)
        self.presenter?.recuperaTempoNotas(aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
    }
    
    func configuraTableView(){
        self.viewAssociada.tableView.register(SelecaoDisciplinaTableViewCell.self, forCellReuseIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.separatorStyle = .none
    }
    
    func definirPeriodosComoNaoSelecionado(){
        for tempoNota in self.tempoNotas {
            tempoNota.estaSelecionado = false
        }
    }
    
    func abreFormularioNota(tempoNota: TempoNota){
        guard let parametros = self.parametros else { return }
        let formularioNotaViewControllerModel: FormularioNotaViewControllerModel = FormularioNotaViewControllerModel(periodo: parametros.periodo, titulo: parametros.titulo, descricao: parametros.descricao, tempoNota: tempoNota, formularioNotaViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
        self.mainCoordinator?.abreFormularioNota(formularioNotaViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionaTempoNotaViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tempoNotas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel, for: indexPath) as? SelecaoDisciplinaTableViewCell {
            cell.populaCelula(tempoNota: self.tempoNotas[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        definirPeriodosComoNaoSelecionado()
        self.tempoNotas[indexPath.row].estaSelecionado = true
        if let tempoNotaSelecionado = self.tempoNotas.tempoNotaSelecionado(), tempoNotaSelecionado.estaSelecionado {
            self.viewAssociada.definirStatusBotaoAoSelecionarUmTempoNota()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
}

extension FormularioSelecionaTempoNotaViewController: FormularioSelecionaTempoNotaViewResponder {
    func botaoProsseguirPressionado() {
        guard let tempoNotaSelecionado = self.tempoNotas.tempoNotaSelecionado() else { return }
        abreFormularioNota(tempoNota: tempoNotaSelecionado)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionaTempoNotaViewController: FormularioSelecionaTempoNotaOutput {
    func retornaTempoNota(tempoNotas: [TempoNota]) {
        self.tempoNotas = tempoNotas
        self.viewAssociada.tableView.reloadData()
    }
}

extension FormularioSelecionaTempoNotaViewController: FormularioNotaViewControllerResponder {
    func botaoProsseguirAoAdicionarNotaPressionado() {
        self.dismiss(animated: true){
            self.parametros?.formularioSelecionaTempoNotaViewControllerResponder?.botaoProsseguirAoSelecionarTempoNotaPressionado()
        }
    }
}
