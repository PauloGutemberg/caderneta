//
//  FormularioDescricaoNotaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/01/21.
//

import UIKit

class FormularioDescricaoNotaViewController: BaseViewController<FormularioDescricaoNotaView> {
    
    var parametros: FormularioDescricaoNotaViewControllerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
    }
    
    func abreFormularioTempoNota(descricao: String){
        guard let parametros = self.parametros else { return }
        let formularioSelecionaTempoNotaViewControllerModel: FormularioSelecionaTempoNotaViewControllerModel = FormularioSelecionaTempoNotaViewControllerModel(periodo: parametros.periodo, titulo: parametros.titulo, descricao: descricao, formularioSelecionaTempoNotaViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
        
        self.mainCoordinator?.abreFormularioTempoNota(formularioSelecionaTempoNotaViewControllerModel, chamador: self)
    }
}

extension FormularioDescricaoNotaViewController: FormularioDescricaoNotaViewResponder {
    func botaoProsseguirPressionado(texto: String) {
        print(texto)
        abreFormularioTempoNota(descricao: texto)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioDescricaoNotaViewController: FormularioSelecionaTempoNotaViewControllerResponder {
    func botaoProsseguirAoSelecionarTempoNotaPressionado() {
        self.dismiss(animated: true) {
            self.parametros?.formularioDescricaoNotaViewControllerResponder?.botaoProsseguirAoEscreverUmaDescricaoPressionado()
        }
    }
}
