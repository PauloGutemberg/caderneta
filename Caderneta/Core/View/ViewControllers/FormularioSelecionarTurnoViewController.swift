//
//  FormularioSelecionarTurnoViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import UIKit

class FormularioSelecionarTurnoViewController: BaseViewController<FormularioSelecionarTurnoView> {
    
    var parametros: FormularioSelecionarTurnoViewControllerModel?
    var presenter: FormularioSelecionarTurnoPresenter?
    var turnos: [Turno] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
        self.presenter = FormularioSelecionarTurnoPresenter(output: self)
        self.presenter?.recuperaTurnos()
        configuraTableView()
    }
    
    func configuraTableView(){
        self.viewAssociada.tableView.register(SelecaoDisciplinaTableViewCell.self, forCellReuseIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.separatorStyle = .none
    }
    
    func definirturnosComoNaoSelecionada(){
        for turno in self.turnos {
            turno.estaSelecionado = false
        }
    }
    
    func abreTelaAdicionarSigla(turno: Turno){
        guard let parametros = self.parametros else { return }
        let formularioSiglaViewControllerModel: FormularioSiglaViewControllerModel = FormularioSiglaViewControllerModel(disciplinas: parametros.disciplinas, ano: parametros.ano, turno: turno, formularioSiglaViewControllerResponder: self, escola: parametros.escola)
        self.mainCoordinator?.abreFormularioAdicionarSigla(formularioSiglaViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionarTurnoViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.turnos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SelecaoDisciplinaTableViewCell.identificadorReusavel, for: indexPath) as? SelecaoDisciplinaTableViewCell {
            cell.populaCelula(turno: turnos[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        definirturnosComoNaoSelecionada()
        self.turnos[indexPath.row].estaSelecionado = true
        if let turnoSelecionado = self.turnos.turnoSelecionado(), turnoSelecionado.estaSelecionado {
            self.viewAssociada.definirStatusBotaoAoSelecionarUmAno()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AtributosDeLayout.capturaValorResponsivo(64)
    }
}

extension FormularioSelecionarTurnoViewController: FormularioSelecionarTurnoOutput {
    
    func retornaTurnos(turnos: [Turno]) {
        self.turnos = turnos
        self.viewAssociada.tableView.reloadData()
    }
}

extension FormularioSelecionarTurnoViewController: FormularioSelecionarTurnoViewResponder {
    func botaoProsseguirPressionado() {
        guard let turnoSelecionado = self.turnos.turnoSelecionado() else { return }
        abreTelaAdicionarSigla(turno: turnoSelecionado)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionarTurnoViewController: FormularioSiglaViewControllerResponder {
    func botaoProsseguirAoEscreverUmaSiglaPressionado() {
        guard let parametros = self.parametros else { return }
        dismiss(animated: true) {
            parametros.formularioSelecionarTurnoViewControllerResponder?.botaoProsseguirAoSelecionarTurnoPressionado()
        }
    }
}
