//
//  ControleDeNavegacaoCustomizado.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/12/20.
//

import UIKit

protocol ControleDeNavegacaoCustomizadoResponder {
    func menuLateralPressionado(sender: UIView)
    func dismissCustomizado() -> Bool
}

class ControleDeNavegacaoCustomizado: UINavigationController {
    
    var responder: ControleDeNavegacaoCustomizadoResponder?
    var controllerAtual: UIViewController?
    var controleDeNavegacaoCustomizado: Bool = true
    let botaoVoltar: UIButton = UIButton()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return controleDeNavegacaoCustomizado ? .lightContent: .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.isEnabled = false
        configNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configuraCor()
    }
    
    func configuraCor(){
        guard self.controleDeNavegacaoCustomizado else {
            return
        }
        navigationBar.barTintColor = SelecionadorDeCor.barraDeNavegacao.cor
    }
    
    func configNavigationBar() {
        navigationBar.barStyle = .default
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.botaoVoltar.frame = CGRect(x: 0, y: 0, width: navigationBar.frame.width * 0.15, height: navigationBar.frame.height)
        self.botaoVoltar.addTarget(self, action: #selector(voltarPressionado), for: UIControl.Event.touchUpInside)
        navigationBar.addSubview(self.botaoVoltar)
        self.botaoVoltar.isHidden = true
    }
    
    func configuraBarraDeFerramentaFilha(controller: UIViewController){
        self.navigationBar.tintColor = UIColor.white
        guard self.controleDeNavegacaoCustomizado else {
            configuraBarraDeFerramentaFilhaModoPadrao()
            return
        }
        
        let stackViewComponentes: UIStackView = UIStackView()
        stackViewComponentes.distribution = .equalCentering
        stackViewComponentes.spacing = 6
        stackViewComponentes.snp.makeConstraints { (make) in
            make.height.equalTo(navigationBar.frame.height * 0.75)
        }
        
        if let imagemParaTitulo = controller.imagemParaTitulo {
            let imagemTitulo = ImagemViewRedonda(image: imagemParaTitulo)
            imagemTitulo.contentMode = .scaleAspectFit
            imagemTitulo.snp.makeConstraints { (make) in
                make.width.equalTo(imagemTitulo.snp.height)
            }
            stackViewComponentes.addArrangedSubview(imagemTitulo)
        }
        
        let labelTitulo = UILabel()
        labelTitulo.accessibilityLabel = accessibilityLabel
        labelTitulo.textColor = UIColor.white
        labelTitulo.font = UIFont.medium(size: 17)
        labelTitulo.text = controller.titulo
        controller.title = ""
        stackViewComponentes.addArrangedSubview(labelTitulo)
        
        self.botaoVoltar.isHidden = false
        controller.navigationItem.titleView = UIView()
        controller.navigationItem.leftItemsSupplementBackButton = true
        controller.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: stackViewComponentes)
    }
    
    func configuraBarraDeFerramentasModoRoot(controller: UIViewController){
        controller.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(voltarPressionado))
        if controller is ConfiguracoesViewController {
            self.controleDeNavegacaoCustomizado = false
        }
        
        guard self.controleDeNavegacaoCustomizado else { return }
        let imagemEsquerda = UIImageView(image: UIImage(named: "caderneta-logo"))
        imagemEsquerda.contentMode = .scaleAspectFit
        imagemEsquerda.snp.makeConstraints { (make) in
            make.width.equalTo(imagemEsquerda.snp.height).multipliedBy(3)
        }
        
        let botaoMenuLateral = UIButton()
        botaoMenuLateral.setImage(UIImage(named: "ic_menu_lateral"), for: .normal)
        botaoMenuLateral.addTarget(self,
                                   action: #selector(ControleDeNavegacaoCustomizado.menuLateralPressionado(sender:)),
                                   for: .touchUpInside)
        
        controller.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: imagemEsquerda)
        self.botaoVoltar.isAccessibilityElement = false
    }
    
    func configuraBarraDeFerramentaFilhaModoPadrao(){
        self.navigationBar.tintColor = .black
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    @objc func menuLateralPressionado(sender: UIButton){
        responder?.menuLateralPressionado(sender: sender)
    }
    
    @objc func voltarPressionado(){
        guard visibleViewController != viewControllers.first else {
            return
        }
        if let viewController = self.controllerAtual as? ControleDeNavegacaoCustomizadoResponder {
            if !viewController.dismissCustomizado() {
                popViewController(animated: true)
            }
        }else{
            popViewController(animated: true)
        }
    }
}

extension ControleDeNavegacaoCustomizado: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.controllerAtual = viewController
        let primeiraViewController = viewControllers.first
        if primeiraViewController == viewController || (viewController as? ListaEscolasViewController != nil){
            configuraBarraDeFerramentasModoRoot(controller: viewController)
        }else{
            configuraBarraDeFerramentaFilha(controller: viewController)
        }
    }
}
