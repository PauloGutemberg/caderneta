//
//  FormularioTituloNotaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/01/21.
//

import Foundation

class FormularioTituloNotaViewController: BaseViewController<FormularioTituloNotaView> {
    
    var parametros: FormularioTituloNotaViewControllerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
    }
    
    func abreFormularioDescricaoNota(titulo: String){
        guard let parametros = self.parametros else { return }
        let formularioDescricaoNotaViewControllerModel: FormularioDescricaoNotaViewControllerModel = FormularioDescricaoNotaViewControllerModel(periodo: parametros.periodo, titulo: titulo, formularioDescricaoNotaViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
        self.mainCoordinator?.abreFormularioDescricaoNota(formularioDescricaoNotaViewControllerModel, chamador: self)
    }
}

extension FormularioTituloNotaViewController: FormularioTituloNotaViewResponder {
    func botaoProsseguirPressionado(texto: String) {
       print(texto)
        abreFormularioDescricaoNota(titulo: texto)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioTituloNotaViewController: FormularioDescricaoNotaViewControllerResponder {
    func botaoProsseguirAoEscreverUmaDescricaoPressionado() {
        self.dismiss(animated: true) {
            self.parametros?.formularioTituloNotaViewControllerResponder?.botaoProsseguirAoEscreverUmTituloPressionado()
        }
    }
}
