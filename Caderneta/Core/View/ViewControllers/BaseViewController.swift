//
//  BaseViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit

open class BaseViewController<V: CodableView>: NiblessViewController where V: UIView {

    override open var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    weak var mainCoordinator: MainCoordinator?
    
    public var viewAssociada: V! {
        return self.view as? V
    }
    
    open override func loadView() {
        self.view = V()
    }
    
    convenience init(mainCoordinator: MainCoordinator) {
        self.init()
        self.mainCoordinator = mainCoordinator
    }
}
