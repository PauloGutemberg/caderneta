//
//  FormularioSelecionaDataViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 31/12/20.
//

import UIKit

class FormularioSelecionaDataViewController: BaseViewController<FormularioSelecionaDataView> {
    
    var parametros: FormularioSelecionaDataViewControllerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
    }
    
    func abreFormularioNome(data: String){
        guard let parametros = self.parametros else { return }
        let formularioNomeViewControllerModel: FormularioNomeViewControllerModel = FormularioNomeViewControllerModel(genero: parametros.genero, dataNascimento: data, formularioNomeViewControllerResponder: self, turma: parametros.turma)
        self.mainCoordinator?.abreFormularioNome(formularioNomeViewControllerModel, chamador: self)
    }
}

extension FormularioSelecionaDataViewController: FormularioSelecionaDataViewResponder {
    func botaoProsseguirPressionado(data: String) {
        print(data)
        abreFormularioNome(data: data)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioSelecionaDataViewController: FormularioNomeViewControllerResponder {
    func botaoProsseguirAoEscreverUmNomePressionado() {
        guard let parametros = self.parametros else { return }
        self.dismiss(animated: true) {
            parametros.formularioSelecionaDataViewControllerResponder?.botaoProsseguirAoSelecionarDataPressionado()
        }
    }
}
