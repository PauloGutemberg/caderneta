//
//  TabBarViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/12/20.
//

import UIKit

protocol TabBarViewControllerResponder {
    
}

class TabBarViewController: UITabBarController {

    var mainCoordinator: MainCoordinator
    var responder: TabBarViewControllerResponder?
    
    init(mainCoordinator: MainCoordinator,  modalPresentationStyle: UIModalPresentationStyle = .fullScreen) {
        self.mainCoordinator = mainCoordinator
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = modalPresentationStyle
        self.delegate = self
        self.configuraControllers()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isTranslucent = false
    }

    func configuraControllers(){
        self.tabBar.isTranslucent = true
        let listaEscolasViewController = ListaEscolasViewController()
        listaEscolasViewController.mainCoordinator = self.mainCoordinator
        listaEscolasViewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "caderno-logo"), tag: 0)
        
        
        let navControllerListaEscolas = ControleDeNavegacaoCustomizado(rootViewController: listaEscolasViewController)
        navControllerListaEscolas.delegate = navControllerListaEscolas
        
        
        /*let configuracoesViewController = ConfiguracoesViewController()
        configuracoesViewController.mainCoordinator = self.mainCoordinator
        configuracoesViewController.tabBarItem = UITabBarItem(title: "Coordernar", image: UIImage(named: "coordenar-logo"), tag: 1)
        let navControllerConfiguracoes = ControleDeNavegacaoCustomizado(rootViewController: configuracoesViewController)
        navControllerConfiguracoes.navigationBar.prefersLargeTitles = true*/
        
        viewControllers = [navControllerListaEscolas]
    }
}

extension TabBarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let controleCustomizado = viewController as? ControleDeNavegacaoCustomizado {
            mainCoordinator.controleDeNavegacao = controleCustomizado
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let customNavigationController = viewController as? ControleDeNavegacaoCustomizado {
            mainCoordinator.controleDeNavegacao = customNavigationController
        }
    }
}
