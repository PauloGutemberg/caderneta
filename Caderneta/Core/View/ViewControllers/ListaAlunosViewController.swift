//
//  ListaAlunosViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import UIKit

class ListaAlunosViewController: BaseViewController<ListaAlunosView> {

    var parametros: ListaAlunosViewControllerModel?
    var presenter: ListaAlunosPresenter?
    var alunos: [Aluno] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.tableView.register(ListaTableViewCell.self, forCellReuseIdentifier: ListaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.responder = self
        self.presenter = ListaAlunosPresenter(output: self)
        self.presenter?.recuperaAlunos(ids: self.parametros?.turma.alunos ?? [])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configuraTitulo()
    }
    
    func configuraTitulo() {
        titulo = "Alunos"
    }
    
}

extension ListaAlunosViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alunos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.viewAssociada.tableView.dequeueReusableCell(withIdentifier: ListaTableViewCell.identificadorReusavel, for: indexPath) as? ListaTableViewCell else {
            return UITableViewCell()
        }
        cell.populaCelula(aluno: self.alunos[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let aluno = self.alunos[seguro: indexPath.row] else {
            return
        }
        guard let parametros = self.parametros else {
            return
        }
        self.mainCoordinator?.abreTelaListaDeNotas(ListaNotasViewControllerModel(turma: parametros.turma, aluno: aluno))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.width * 76/360
    }
}

extension ListaAlunosViewController: ListaAlunosOutput {
    func retornaTurmaAssociada(turma: Turma) {
        self.presenter?.recuperaAlunos(ids: turma.alunos)
    }
    
    func retornaAlunos(alunos: [Aluno]) {
        self.alunos = alunos
        self.viewAssociada.tableView.reloadData()
    }
}

extension ListaAlunosViewController: ListaAlunosViewResponder {
    func adicionarPressionado() {
        guard let parametros = self.parametros else {
            return
        }
        let formularioSelecionaGeneroViewControllerModel: FormularioSelecionaGeneroViewControllerModel = FormularioSelecionaGeneroViewControllerModel(formularioSelecionaGeneroViewControllerResponder: self, turma: parametros.turma)
        self.mainCoordinator?.abreFormularioSelecionaGenero(formularioSelecionaGeneroViewControllerModel, chamador: self)
    }
}

extension ListaAlunosViewController: FormularioSelecionaGeneroViewControllerResponder {

    func botaoProsseguirAoSelecionarGeneroPressionado(){
        self.presenter?.recuperaTurmaAssociada(id: self.parametros?.turma.id ?? "")
    }
}
