//
//  TelaInicialViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/11/20.
//

import UIKit

class TelaInicialViewController: BaseViewController<TelaInicialView> {

    var parametros:  TelaInicialViewControllerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainCoordinator?.abreListaDeEscolas()
    }
}
