//
//  ListaSalasDeAulaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/12/20.
//

import UIKit

class ListaSalasDeAulaViewController: BaseViewController<ListaSalasDeAulaView> {

    var parametros: ListaSalasDeAulaViewControllerModel?
    var presenter: ListaTurmasPresenter?
    var salasDeAula: [Turma] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.tableView.register(ListaTableViewCell.self, forCellReuseIdentifier: ListaTableViewCell.identificadorReusavel)
        self.viewAssociada.tableView.dataSource = self
        self.viewAssociada.tableView.delegate = self
        self.viewAssociada.responder = self
        self.presenter = ListaTurmasPresenter(output: self)
        self.presenter?.recuperarTurmas(ids: self.parametros?.escola.turmas ?? [])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configuraTitulo()
    }
    
    func configuraTitulo() {
        titulo = "Salas"
    }
}

extension ListaSalasDeAulaViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salasDeAula.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.viewAssociada.tableView.dequeueReusableCell(withIdentifier: ListaTableViewCell.identificadorReusavel, for: indexPath) as? ListaTableViewCell else {
            return UITableViewCell()
        }
        cell.populaCelula(turma: self.salasDeAula[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let turma = self.salasDeAula[seguro: indexPath.row] else {
            return
        }
        
        self.mainCoordinator?.abreTelaListaDeAlunos(ListaAlunosViewControllerModel(turma: turma))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.width * 76/360
    }
}

extension ListaSalasDeAulaViewController: ListaViewResponder {
    func adicionarPressionado() {
        guard let parametros = self.parametros else {
            return
        }
        let formularioSelecionarDisciplinaViewControllerModel: FormularioSelecionarDisciplinaViewControllerModel = FormularioSelecionarDisciplinaViewControllerModel(formularioSelecionarDisciplinaViewControllerResponder: self, escola: parametros.escola)
        self.mainCoordinator?.abreFormularioSelecionarDisciplina(formularioSelecionarDisciplinaViewControllerModel, chamador: self)
    }
}

extension ListaSalasDeAulaViewController: ListaTurmasOutput {
    func retornaTurmas(turmas: [Turma]) {
        self.salasDeAula = turmas
        self.viewAssociada.tableView.reloadData()
    }
    
    func retornaEscolaAssociada(escola: Escola){
        self.presenter?.recuperarTurmas(ids: escola.turmas)
    }
    
}

extension ListaSalasDeAulaViewController: FormularioSelecionarDisciplinaViewControllerResponder {
    func botaoProsseguirAoSelecionarDisciplinaPressionado() {
        self.presenter?.recuperaEscolaAssociada(id: self.parametros?.escola.id ?? "")
    }
}
