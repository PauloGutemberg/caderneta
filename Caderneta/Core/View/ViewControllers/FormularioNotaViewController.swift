//
//  FormularioNotaViewController.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import UIKit

class FormularioNotaViewController: BaseViewController<FormularioNotaView> {
    
    var parametros: FormularioNotaViewControllerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAssociada.responder = self
    }
    
    func abreFormularioDataNota(nota: Double){
        guard let parametros = self.parametros else { return }
        let formularioDataNotaViewControllerModel: FormularioDataNotaViewControllerModel = FormularioDataNotaViewControllerModel(periodo: parametros.periodo, titulo: parametros.titulo, descricao: parametros.descricao, tempoNota: parametros.tempoNota, nota: nota, formularioDataNotaViewControllerResponder: self, aluno: parametros.aluno, turma: parametros.turma, disciplina: parametros.disciplina)
        self.mainCoordinator?.abreFormularioDataNota(formularioDataNotaViewControllerModel, chamador: self)
    }
}

extension FormularioNotaViewController: FormularioSiglaViewResponder {
    
    func botaoProsseguirPressionado(texto: String) {
        let notaString = texto.replacingOccurrences(of: ",", with: ".")
        guard let nota = Double(notaString) else { return }
        abreFormularioDataNota(nota: nota)
    }
    
    func botaoFecharPressionado() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormularioNotaViewController: FormularioSelecionaDataViewControllerResponder {
    func botaoProsseguirAoSelecionarDataPressionado() {
        self.dismiss(animated: true){
            self.parametros?.formularioNotaViewControllerResponder?.botaoProsseguirAoAdicionarNotaPressionado()
        }
    }
}
