//
//  ProvedorTipo.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 10/01/21.
//

import Foundation

public protocol ProvedorTipo {
    var descricaoFormato: String { get }
}
