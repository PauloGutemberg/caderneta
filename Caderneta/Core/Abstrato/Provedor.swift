//
//  Provedor.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 10/01/21.
//

import Foundation
import RealmSwift

public protocol RealmProvedor: Provedor {
    var fonte: Realm { get set }
}

public protocol Provedor { //repositorio
    associatedtype T: ProvedorTipo
    var  valores: [T] { get set }
    func inserir(_ object: T)
    func deletar(_ object: T)
    func atualizar(_ object: T)
    func obter(_ withID: String) -> T?
}
