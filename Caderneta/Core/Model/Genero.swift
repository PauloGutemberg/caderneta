//
//  Genero.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 30/12/20.
//

import Foundation

class Genero {
    
    var descricao: String
    var caminhoImagem: String
    
    init(descricao: String, caminhoImagem: String) {
        self.descricao = descricao
        self.caminhoImagem = caminhoImagem
    }
}
