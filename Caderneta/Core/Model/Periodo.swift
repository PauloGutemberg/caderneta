//
//  Periodo.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 06/01/21.
//

import Foundation

class Periodo{
    
    var id: Int
    var descricao: String
    var estaSelecionado: Bool
    
    init(id: Int, descricao: String, estaSelecionado: Bool) {
        self.id = id
        self.descricao = descricao
        self.estaSelecionado = estaSelecionado
    }
}
