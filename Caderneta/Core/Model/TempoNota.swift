//
//  TempoNota.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import Foundation

class TempoNota {
    
    var id: Int
    var descricao: String
    var estaSelecionado: Bool
    
    init(id: Int, descricao: String, estaSelecionado: Bool) {
        self.id = id
        self.descricao = descricao
        self.estaSelecionado = estaSelecionado
    }
}
