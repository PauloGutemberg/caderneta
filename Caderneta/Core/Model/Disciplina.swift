//
//  Disciplina.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/12/20.
//

import Foundation

class Disciplina: ProvedorTipo {
    
    var id: String
    var nome: String
    var estaSelecionada: Bool = false

    public var descricaoFormato: String {
        return self.nome
    }
    
    init(nome: String, estaSelecionada: Bool, id: String) {
        self.nome = nome
        self.estaSelecionada = estaSelecionada
        self.id = id
    }
    
    init(realmObjeto: DisciplinaRealm){
        self.id = realmObjeto.id
        self.nome = realmObjeto.nome
    }
}
