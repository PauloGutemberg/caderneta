//
//  Turma.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

class Turma: ProvedorTipo {
    
    var id: String
    var sigla: String
    var ano: Ano
    var turno: Turno
    var disciplinas: Array<String>
    var caminhoImagem: String?
    var alunos: Array<String>
    var participacoesNaTurma: Array<String>
    
    public var descricaoFormato: String {
        return self.sigla
    }
    
    init(sigla: String, ano: Ano, turno: Turno, disciplinas: Array<String>, caminhoImagem: String?, id: String, participacoesNaTurma: Array<String>, alunos: Array<String>){
        self.sigla = sigla
        self.ano = ano
        self.turno = turno
        self.disciplinas = disciplinas
        self.caminhoImagem = caminhoImagem
        self.id = id
        self.alunos =  alunos
        self.participacoesNaTurma = participacoesNaTurma
    }
    
    init(realmObjeto: TurmaRealm){
        self.id = realmObjeto.id
        self.sigla = realmObjeto.sigla
        self.ano = Ano(descricao: realmObjeto.ano)
        self.turno = Turno(descricao: realmObjeto.turno)
        var disciplinas = Array<String>()
        realmObjeto.disciplinas.forEach({
            disciplinas.append($0)
        })
        self.disciplinas = disciplinas
        self.caminhoImagem = realmObjeto.caminhoImagem
        var alunos = Array<String>()
        realmObjeto.alunos.forEach({
            alunos.append($0)
        })
        self.alunos = alunos
        var participacoesNaTurma = Array<String>()
        realmObjeto.participacoesNaTurma.forEach({
            participacoesNaTurma.append($0)
        })
        self.participacoesNaTurma = participacoesNaTurma
    }
}
