//
//  FormularioNomeViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 01/01/21.
//

import Foundation

struct FormularioNomeViewControllerModel {
    
    var genero: Genero
    var dataNascimento: String
    var formularioNomeViewControllerResponder: FormularioNomeViewControllerResponder?
    var turma: Turma
}
