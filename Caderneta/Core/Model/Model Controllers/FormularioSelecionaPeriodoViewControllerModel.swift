//
//  FormularioSelecionaPeriodoViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 06/01/21.
//

import Foundation

struct FormularioSelecionaPeriodoViewControllerModel {
   
    var formularioSelecionaPeriodoViewControllerResponder: FormularioSelecionaPeriodoViewControllerResponder?
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina
}
