//
//  ListaNotasViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import Foundation

struct ListaNotasViewControllerModel {
    var turma: Turma
    var aluno: Aluno
}
