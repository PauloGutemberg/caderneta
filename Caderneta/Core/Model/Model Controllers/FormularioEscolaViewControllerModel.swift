//
//  FormularioEscolaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 12/12/20.
//

import Foundation

struct FormularioEscolaViewControllerModel {
    
    var estaEditandoDados: Bool = false
    var formularioEscolaViewControllerResponder: FormularioEscolaViewControllerResponder?
    
}
