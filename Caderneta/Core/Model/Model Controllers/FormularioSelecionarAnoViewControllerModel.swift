//
//  FormularioSelecionarAnoViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/12/20.
//

import Foundation

struct FormularioSelecionarAnoViewControllerModel {
    var disciplinas: [Disciplina]
    var formularioSelecionarAnoViewControllerResponder: FormularioSelecionarAnoViewControllerResponder?
    var escola: Escola
}
