//
//  FormularioDescricaoNotaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 08/01/21.
//

import Foundation

struct FormularioDescricaoNotaViewControllerModel {
    
    var periodo: Periodo
    var titulo: String
    
    var formularioDescricaoNotaViewControllerResponder: FormularioDescricaoNotaViewControllerResponder?
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina

}
