//
//  FormularioSelecionaGeneroViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 28/12/20.
//

import Foundation

struct FormularioSelecionaGeneroViewControllerModel {
    
    var formularioSelecionaGeneroViewControllerResponder: FormularioSelecionaGeneroViewControllerResponder?
    var turma: Turma
}
