//
//  FormularioSelecionaTempoNotaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import Foundation

struct FormularioSelecionaTempoNotaViewControllerModel {
    var periodo: Periodo
    var titulo: String
    var descricao: String
    
    var formularioSelecionaTempoNotaViewControllerResponder: FormularioSelecionaTempoNotaViewControllerResponder?
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina

}
