//
//  FormularioSelecionarDisciplinaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/12/20.
//

import Foundation

struct FormularioSelecionarDisciplinaViewControllerModel {
    
    var formularioSelecionarDisciplinaViewControllerResponder: FormularioSelecionarDisciplinaViewControllerResponder?
    var escola: Escola
}
