//
//  FormularioSelecionarTurnoViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

struct FormularioSelecionarTurnoViewControllerModel {
    var disciplinas: [Disciplina]
    var ano: Ano
    var formularioSelecionarTurnoViewControllerResponder: FormularioSelecionarTurnoViewControllerResponder?
    var escola: Escola
}
