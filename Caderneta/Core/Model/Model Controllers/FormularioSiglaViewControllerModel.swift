//
//  FormularioSiglaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

struct FormularioSiglaViewControllerModel {
    
    var disciplinas: [Disciplina]
    var ano: Ano
    var turno: Turno
    var formularioSiglaViewControllerResponder: FormularioSiglaViewControllerResponder?
    var escola: Escola
}
