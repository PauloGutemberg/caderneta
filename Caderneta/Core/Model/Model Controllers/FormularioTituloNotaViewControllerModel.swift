//
//  FormularioTituloNotaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 07/01/21.
//

import Foundation

struct FormularioTituloNotaViewControllerModel {
   
    var periodo: Periodo
    
    var formularioTituloNotaViewControllerResponder: FormularioTituloNotaViewControllerResponder?
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina
}
