//
//  FormularioDataNotaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import Foundation

struct FormularioDataNotaViewControllerModel {
    var periodo: Periodo
    var titulo: String
    var descricao: String
    var tempoNota: TempoNota
    var nota: Double
    
    var formularioDataNotaViewControllerResponder: FormularioSelecionaDataViewControllerResponder?
    
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina
}
