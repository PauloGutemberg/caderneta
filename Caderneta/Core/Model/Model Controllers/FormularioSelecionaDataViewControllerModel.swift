//
//  FormularioSelecionaDataViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 01/01/21.
//

import Foundation

struct FormularioSelecionaDataViewControllerModel {
    
    var genero: Genero
    var formularioSelecionaDataViewControllerResponder: FormularioSelecionaDataViewControllerResponder?
    var turma: Turma
}
