//
//  FormularioNotaViewControllerModel.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import Foundation

struct  FormularioNotaViewControllerModel {
    var periodo: Periodo
    var titulo: String
    var descricao: String
    var tempoNota: TempoNota
    
    var formularioNotaViewControllerResponder: FormularioNotaViewControllerResponder?
    var aluno: Aluno
    var turma: Turma
    var disciplina: Disciplina

}
