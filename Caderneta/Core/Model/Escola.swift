//
//  Escola.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import Foundation

class Escola: ProvedorTipo {
    
    var id: String
    var nome: String
    var caminhoImagem: String?
    
    var turmas: Array<String>
    public var descricaoFormato: String {
        return self.nome
    }
    
    init(nome: String, caminhoImagem: String?, id: String, turmas: Array<String>) {
        self.id = id
        self.nome = nome
        self.caminhoImagem = caminhoImagem
        self.turmas = turmas
    }
    
    init(realmObjeto: EscolaRealm) {
        self.id = realmObjeto.id
        self.nome = realmObjeto.nome
        self.caminhoImagem = realmObjeto.caminhoImagem
        var turmas = Array<String>()
        realmObjeto.turmas.forEach({
            turmas.append($0)
        })
        self.turmas = turmas
    }
}
