//
//  ParticipacaoNaTurma.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/01/21.
//

import Foundation

class ParticipacaoNaTurma: ProvedorTipo {
    
    var id: String
    var idDisciplina: String
    var idAluno: String
    var idTurma: String
    
    var primeiroPeriodo: Array<String>
    var segundoPeriodo: Array<String>
    var terceiroPeriodo: Array<String>
    var quartoPeriodo: Array<String>
    var recuperacao: Array<String>
    
    public var descricaoFormato: String {
        return self.id
    }
    
    init(id: String, idDisciplina: String, idAluno: String, idTurma: String, primeiroPeriodo: Array<String>, segundoPeriodo: Array<String>, terceiroPeriodo: Array<String>, quartoPeriodo: Array<String>, recuperacao: Array<String>){
        self.id = id
        self.idDisciplina = idDisciplina
        self.idAluno = idAluno
        self.idTurma = idTurma
        self.primeiroPeriodo = primeiroPeriodo
        self.segundoPeriodo = segundoPeriodo
        self.terceiroPeriodo = terceiroPeriodo
        self.quartoPeriodo = quartoPeriodo
        self.recuperacao = recuperacao
    }
    
    init(realmObjeto: ParticipacaoNaTurmaRealm){
        self.id = realmObjeto.id
        self.idDisciplina = realmObjeto.idDisciplina
        self.idAluno = realmObjeto.idAluno
        self.idTurma = realmObjeto.idTurma
        
        var primeiroPeriodo = Array<String>()
        realmObjeto.primeiroPeriodo.forEach({primeiroPeriodo.append($0)})
        self.primeiroPeriodo = primeiroPeriodo
        
        var segundoPeriodo = Array<String>()
        realmObjeto.segundoPeriodo.forEach({segundoPeriodo.append($0)})
        self.segundoPeriodo = segundoPeriodo
        
        var terceiroPeriodo = Array<String>()
        realmObjeto.terceiroPeriodo.forEach({terceiroPeriodo.append($0)})
        self.terceiroPeriodo = terceiroPeriodo
        
        var quartoPeriodo = Array<String>()
        realmObjeto.quartoPeriodo.forEach({quartoPeriodo.append($0)})
        self.quartoPeriodo = quartoPeriodo
        
        var recuperacao = Array<String>()
        realmObjeto.recuperacao.forEach({recuperacao.append($0)})
        self.recuperacao = recuperacao

    }
}
