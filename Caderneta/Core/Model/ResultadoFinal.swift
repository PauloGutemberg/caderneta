//
//  ResultadoFinal.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/01/21.
//

import Foundation

class ResultadoFinal {
    
    var disciplina: Disciplina
    var nota: Nota
    
    init(disciplina: Disciplina, nota: Nota) {
        self.disciplina = disciplina
        self.nota = nota
    }
}
