//
//  EscolaRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 10/01/21.
//

import Foundation
import RealmSwift

class EscolaRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var nome: String = ""
    @objc dynamic var caminhoImagem: String?
    
    var turmas: List<String> = List<String>()
    
    static func com(objetoAplicacao: Escola) -> EscolaRealm {
        let objRealm = EscolaRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.nome = objetoAplicacao.nome
        objRealm.caminhoImagem = objetoAplicacao.caminhoImagem
        
        objetoAplicacao.turmas.forEach({objRealm.turmas.append($0)})
        
        return objRealm
    }
}
