//
//  AlunoRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/01/21.
//

import Foundation
import RealmSwift

class AlunoRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var nome: String = ""
    @objc dynamic var dataNascimento: String = ""
    @objc dynamic var genero: String = ""
    @objc dynamic var caminhoImagem: String?
    
    static func com(objetoAplicacao: Aluno) -> AlunoRealm{
        let objRealm = AlunoRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.nome = objetoAplicacao.nome
        objRealm.dataNascimento = objetoAplicacao.dataNascimento
        objRealm.genero = objetoAplicacao.genero
        objRealm.caminhoImagem = objetoAplicacao.caminhoImagem
        return objRealm
    }
}
