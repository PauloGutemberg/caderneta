//
//  ParticipacaoNaTurmaRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation
import RealmSwift

class ParticipacaoNaTurmaRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var idDisciplina: String = ""
    @objc dynamic var idAluno: String = ""
    @objc dynamic var idTurma: String = ""
    
    var primeiroPeriodo: List<String> = List<String>()
    var segundoPeriodo: List<String> = List<String>()
    var terceiroPeriodo: List<String> = List<String>()
    var quartoPeriodo: List<String> = List<String>()
    var recuperacao: List<String> = List<String>()
    
    static func com(objetoAplicacao: ParticipacaoNaTurma) -> ParticipacaoNaTurmaRealm {
        let objRealm = ParticipacaoNaTurmaRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.idDisciplina = objetoAplicacao.idDisciplina
        objRealm.idAluno = objetoAplicacao.idAluno
        objRealm.idTurma = objetoAplicacao.idTurma
        
        objetoAplicacao.primeiroPeriodo.forEach({objRealm.primeiroPeriodo.append($0)})
        objetoAplicacao.segundoPeriodo.forEach({objRealm.segundoPeriodo.append($0)})
        objetoAplicacao.terceiroPeriodo.forEach({objRealm.terceiroPeriodo.append($0)})
        objetoAplicacao.quartoPeriodo.forEach({objRealm.quartoPeriodo.append($0)})
        objetoAplicacao.recuperacao.forEach({objRealm.recuperacao.append($0)})
        return objRealm
    }
}
