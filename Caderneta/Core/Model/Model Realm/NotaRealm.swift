//
//  NotaRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/01/21.
//

import Foundation
import RealmSwift

class NotaRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var valor: Double = 0.0
    @objc dynamic var observacao: String = ""
    @objc dynamic var dataNota: String = ""
    @objc dynamic var titulo: String = ""
    @objc dynamic var seguimento: String = ""
    @objc dynamic var periodo: String = ""
    
    static func com(objetoAplicacao: Nota) -> NotaRealm {
        let objRealm = NotaRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.valor = objetoAplicacao.valor
        objRealm.observacao = objetoAplicacao.observacao
        objRealm.dataNota = objetoAplicacao.dataNota
        objRealm.titulo = objetoAplicacao.titulo
        objRealm.seguimento = objetoAplicacao.seguimento
        objRealm.periodo = objetoAplicacao.periodo
        return objRealm
    }
}
