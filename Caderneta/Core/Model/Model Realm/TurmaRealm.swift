//
//  TurmaRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/01/21.
//

import Foundation
import RealmSwift

class TurmaRealm: Object {
   
    @objc dynamic var id: String = ""
    @objc dynamic var sigla: String = ""
    @objc dynamic var ano: String = ""
    @objc dynamic var turno: String = ""
    @objc dynamic var caminhoImagem: String?
    
    var alunos: List<String> = List<String>()
    var disciplinas: List<String> = List<String>()
    var participacoesNaTurma: List<String> = List<String>()
    
    static func com(objetoAplicacao: Turma)-> TurmaRealm {
        let objRealm = TurmaRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.sigla = objetoAplicacao.sigla
        objRealm.ano = objetoAplicacao.ano.descricao
        objRealm.turno = objetoAplicacao.turno.descricao
        objRealm.caminhoImagem = objetoAplicacao.caminhoImagem
        
        objetoAplicacao.alunos.forEach({objRealm.alunos.append($0)})
        objetoAplicacao.disciplinas.forEach({objRealm.disciplinas.append($0)})
        objetoAplicacao.participacoesNaTurma.forEach({objRealm.participacoesNaTurma.append($0)})
        
        return objRealm
    }
}
