//
//  DisciplinaRealm.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation
import RealmSwift

class DisciplinaRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var nome: String = ""

    static func com(objetoAplicacao: Disciplina) -> DisciplinaRealm {
        let objRealm = DisciplinaRealm()
        objRealm.id = objetoAplicacao.id
        objRealm.nome = objetoAplicacao.nome
        return objRealm
    }
}
