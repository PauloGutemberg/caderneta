//
//  Turno.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

class Turno {
    
    var descricao: String
    var estaSelecionado: Bool
    
    init(descricao: String, estaSelecionado: Bool = false) {
        self.descricao = descricao
        self.estaSelecionado = estaSelecionado
    }
}
