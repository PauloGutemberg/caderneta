//
//  Nota.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import Foundation

class Nota: ProvedorTipo {

    var id: String
    var valor: Double
    var observacao: String
    var dataNota: String
    var titulo: String
    var colapso: Bool
    var seguimento: String
    var periodo: String
    
    public var descricaoFormato: String {
        return self.titulo
    }
    
    init(valor: Double, observacao: String, dataNota: String, titulo: String, colapso: Bool, id: String, seguimento: String, periodo: String) {
        self.valor = valor
        self.observacao = observacao
        self.dataNota = dataNota
        self.titulo = titulo
        self.colapso = colapso
        self.id = id
        self.seguimento = seguimento
        self.periodo = periodo
    }
    
    init(realmObjeto: NotaRealm) {
        self.id = realmObjeto.id
        self.valor = realmObjeto.valor
        self.observacao = realmObjeto.observacao
        self.dataNota = realmObjeto.dataNota
        self.titulo = realmObjeto.titulo
        self.colapso = false
        self.seguimento = realmObjeto.seguimento
        self.periodo = realmObjeto.periodo
    }
}
