//
//  Aluno.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import Foundation

class Aluno: ProvedorTipo {
    
    var id: String
    var nome: String
    var dataNascimento: String
    var genero: String
    var caminhoImagem: String?
    
    public var descricaoFormato: String {
        return self.nome
    }
    
    init(nome: String, dataNascimento: String, genero: String ,caminhoImagem: String?, id: String) {
        self.nome = nome
        self.dataNascimento = dataNascimento
        self.genero = genero
        self.caminhoImagem = caminhoImagem
        self.id = id
    }
    
    init(realmObjeto: AlunoRealm){
        self.id = realmObjeto.id
        self.nome = realmObjeto.nome
        self.dataNascimento = realmObjeto.dataNascimento
        self.genero = realmObjeto.genero
        self.caminhoImagem = realmObjeto.caminhoImagem
    }
}
