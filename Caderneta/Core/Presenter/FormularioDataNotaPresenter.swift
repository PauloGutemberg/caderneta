//
//  FormularioDataNotaPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 23/01/21.
//

import Foundation

class FormularioDataNotaPresenter: NSObject {
    
    var output: FormularioDataNotaOutput
    
    init(output: FormularioDataNotaOutput){
        self.output = output
    }
    
    func inserirNota(nota: Nota){
        ServicoConsultaNota(output: self).inserirNota(nota: nota)
    }
    
    func atualizarParticipacao(participacao: ParticipacaoNaTurma){
        ServicoConsultaParticipacaoTurma(output: self).atualizaParticipacao(participacao: participacao)
    }
    
    func obterParticipacao(idTurma: String, idDisciplina: String, idAluno: String){
        ServicoConsultaParticipacaoTurma(output: self).obterParticipacao(idTurma: idTurma, idAluno: idAluno, idDisciplina: idDisciplina)
    }
    
}

extension FormularioDataNotaPresenter: ServicoConsultaNotaOutput {
    func obterNotas(notas: [Nota]){}
}

extension FormularioDataNotaPresenter: ServicoConsultaParticipacaoTurmaOutput {
    func obterParticipacoes(participacoes: [ParticipacaoNaTurma]) {}
    func obterParticipacao(participacao: ParticipacaoNaTurma){
        self.output.obterParticipacao(participacao: participacao)
    }
}
