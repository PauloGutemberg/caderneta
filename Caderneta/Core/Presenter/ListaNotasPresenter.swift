//
//  ListaNotasPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 02/01/21.
//

import Foundation

class ListaNotasPresenter: NSObject {
    
    var output: ListaNotasOutput
    var chavePaginaAssociada: String = ""
    var disciplinaId: String = ""
    var disciplina: Disciplina?
    
    init(output: ListaNotasOutput){
        self.output = output
    }
    
    func recuperarNotas(alunoID: String, turmaID: String, disciplinaID: String, chavePaginaNotasAssociada: String){
        self.chavePaginaAssociada = chavePaginaNotasAssociada
        self.disciplinaId = disciplinaID
        self.disciplina = nil ///reseta sempre a disciplina quando realizar a consulta
        if chavePaginaNotasAssociada == "RESULT. FINAL" {
            recuperaNotasFinais(alunoID: alunoID, turmaID: turmaID, disciplinaID: disciplinaID, chavePaginaNotasAssociada: chavePaginaNotasAssociada)
        }else{
            ServicoConsultaParticipacaoTurma(output: self).obterParticipacao(idTurma: turmaID, idAluno: alunoID, idDisciplina: disciplinaID)
        }
    }
    
    func recuperarDisciplinas(ids: [String]) {
        ServicoConsultaDisciplinas(output: self).obterDisciplinas(por: ids)
    }
    
    func recuperaNotasFinais(alunoID: String, turmaID: String, disciplinaID: String, chavePaginaNotasAssociada: String){
        ServicoConsultaParticipacaoTurma(output: self).obterParticipacao(idTurma: turmaID, idAluno: alunoID, idDisciplina: disciplinaID)
    }
    
    ///Calcular o resultado final por disciplina, uma de cada vez
    func calculaResultadoFinal(notas: [Nota]){
        let notaFinal: Nota = Nota(valor: 0, observacao: "", dataNota: "", titulo: "", colapso: false, id: "", seguimento: "", periodo: "")
        for nota in notas {
            notaFinal.valor = notaFinal.valor + nota.valor
        }
        notaFinal.valor = notaFinal.valor/Double(notas.count)
        ServicoConsultaDisciplinas(output: self).obterDisciplina(id: self.disciplinaId)
        guard let disciplina = self.disciplina else { return }
        let resultadoFinal = ResultadoFinal(disciplina: disciplina, nota: notaFinal)
        self.output.retornaResultadoFinal(resultado: resultadoFinal, chavePaginaNotasAssociada: self.chavePaginaAssociada)
    }
    
    func adicionaCelulaResultadoFinal(disciplinas: [Disciplina]) -> [Disciplina]{
        var disciplinasAuxiliar = disciplinas
        let resultadoFinal = Disciplina(nome: "RESULT. FINAL", estaSelecionada: false, id: "")
        disciplinasAuxiliar.append(resultadoFinal)
        return disciplinasAuxiliar
    }
}

extension ListaNotasPresenter: ServicoConsultaDisciplinasOutput {
    func obterDisciplina(disciplina: Disciplina) {
        self.disciplina = disciplina
    }
    
    func obterDisciplinas(disciplinas: [Disciplina]) {
        self.output.retornaDisciplinas(disciplinas: adicionaCelulaResultadoFinal(disciplinas: disciplinas))
    }
}

extension ListaNotasPresenter: ServicoConsultaParticipacaoTurmaOutput {
    func obterParticipacoes(participacoes: [ParticipacaoNaTurma]) {}
    func obterParticipacao(participacao: ParticipacaoNaTurma) {
        ServicoConsultaNota(output: self).obterNotas(participacao: participacao)
    }
}

extension ListaNotasPresenter: ServicoConsultaNotaOutput {
    func obterNotas(notas: [Nota]) {
        if self.chavePaginaAssociada == "RESULT. FINAL" {
            calculaResultadoFinal(notas: notas)
        }else{
            self.output.retornaNotas(notas: notas, chavePaginaNotasAssociada: self.chavePaginaAssociada)
        }
    }
}
