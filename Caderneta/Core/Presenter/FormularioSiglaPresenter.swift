//
//  FormularioSiglaPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation

class FormularioSiglaPresenter: NSObject {
    
    var output: FormularioSiglaOutput
    
    init(output: FormularioSiglaOutput){
        self.output = output
    }
    
    func inserirTurma(turma: Turma){
        ServicoConsultaTurmas(output: self).inserirTurma(turma: turma)
    }
    
    func atualizaEscolaComNovaTurma(escola: Escola){
        ServicoConsultaEscolas(output: self).atualizaEscola(escola: escola)
    }
}

extension FormularioSiglaPresenter: ServicoConsultaTurmasOutput {
    func obterTurmas(turmas: [Turma]) {}
    func obterTurma(turma: Turma) {}
}

extension FormularioSiglaPresenter: ServicoConsultaEscolasOutput {
    func obterEscolas(escolas: [Escola]) {}
    func obterEscola(escola: Escola) {}
}
