//
//  FormularioSelecionaPeriodoPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 06/01/21.
//

import Foundation

class FormularioSelecionaPeriodoPresenter: NSObject {
    
    var output: FormularioSelecionaPeriodoOutput
    
    init(output: FormularioSelecionaPeriodoOutput) {
        self.output = output
    }
    
    func recuperarPeriodos(){
        output.retornaPeriodos(periodos: mockPeriodos())
    }
    
    func mockPeriodos() -> [Periodo] {
        
        let periodo1 = Periodo(id: 1, descricao: PeriodoDescricao.primeiro.rawValue, estaSelecionado: false)
        let periodo2 = Periodo(id: 2, descricao: PeriodoDescricao.segundo.rawValue, estaSelecionado: false)
        let periodo3 = Periodo(id: 3, descricao: PeriodoDescricao.terceiro.rawValue, estaSelecionado: false)
        let periodo4 = Periodo(id: 4, descricao: PeriodoDescricao.quarto.rawValue, estaSelecionado: false)
        let periodo5 = Periodo(id: 5, descricao: PeriodoDescricao.recuperacao.rawValue, estaSelecionado: false)
        
        return [periodo1, periodo2, periodo3, periodo4, periodo5]
    }
}
