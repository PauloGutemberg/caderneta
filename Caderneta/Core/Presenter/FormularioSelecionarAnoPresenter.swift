//
//  FormularioSelecionarAnoPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

class FormularioSelecionarAnoPresenter: NSObject {
    
    var output: FormularioSelecionarAnoOutput
    
    init(output: FormularioSelecionarAnoOutput){
        self.output = output
    }
    
    func recuperarAnos(){
        var anos: [Ano] = []
        for aux in 20..<50 {
            let descricao = "20\(String(aux))"
            let ano = Ano(descricao: descricao, estaSelecionado: false)
            anos.append(ano)
        }
        output.retornarAnos(anos: anos)
    }
}
