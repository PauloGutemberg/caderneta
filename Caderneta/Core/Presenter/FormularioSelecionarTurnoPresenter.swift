//
//  FormularioSelecionarTurnoPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/12/20.
//

import Foundation

class FormularioSelecionarTurnoPresenter: NSObject {
    
    var output: FormularioSelecionarTurnoOutput
    
    init(output: FormularioSelecionarTurnoOutput){
        self.output = output
    }
    
    func recuperaTurnos(){
        let matutino: Turno = Turno(descricao: "Matutino", estaSelecionado: false)
        let vespetino: Turno = Turno(descricao: "Vespetino", estaSelecionado: false)
        let noturno: Turno = Turno(descricao: "Noturno", estaSelecionado: false)
        output.retornaTurnos(turnos: [matutino, vespetino, noturno])
    }
}
