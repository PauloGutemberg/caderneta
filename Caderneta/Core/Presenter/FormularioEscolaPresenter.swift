//
//  FormularioEscolaPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 10/01/21.
//

import Foundation

class FormularioEscolaPresenter: NSObject {
    
    var output: FormularioEscolaOutput
    
    init(output: FormularioEscolaOutput) {
        self.output = output
    }
    
    func inserirEscola(escola: Escola){
        ServicoConsultaEscolas(output: self).inserirEscola(escola: escola)
    }
}

extension FormularioEscolaPresenter: ServicoConsultaEscolasOutput {
    func obterEscolas(escolas: [Escola]) {}
    func obterEscola(escola: Escola) {}
}
