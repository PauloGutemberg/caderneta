//
//  ListaEscolasPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import Foundation

class ListaEscolasPresenter: NSObject {
    
    var output: ListaEscolasOutput
    
    init(output: ListaEscolasOutput){
        self.output = output
    }
    
    func recuperarEscolas(){
        //self.output.retornaEscolas(escolas: mockEscolas())
        ServicoConsultaEscolas(output: self).obterEscolas()
    }
    
    /*func mockEscolas() -> [Escola] {
        let escola1 = Escola(nome: "Centro de Ensino Newton Bello", caminhoImagem: nil, id: UUID().uuidString, turmas: Array<String>())
        let escola2 = Escola(nome: "Centro de Ensino Joaquim Salviano", caminhoImagem: nil, id: UUID().uuidString, turmas: Array<String>())
        let escola3 = Escola(nome: "Centro de Ensino Aristoteles Pires", caminhoImagem: nil, id: UUID().uuidString, turmas: Array<String>())
       return [escola1, escola2, escola3]
    }*/
}

extension ListaEscolasPresenter: ServicoConsultaEscolasOutput {
    func obterEscolas(escolas: [Escola]) {
        self.output.retornaEscolas(escolas: escolas.sorted {$0.nome < $1.nome})
    }
    func obterEscola(escola: Escola) {}
}
