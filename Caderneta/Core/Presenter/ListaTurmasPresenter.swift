//
//  ListaTurmasPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import Foundation

class ListaTurmasPresenter: NSObject {
    
    var output: ListaTurmasOutput
    
    init(output: ListaTurmasOutput){
        self.output = output
    }
    
    func recuperarTurmas(ids: [String]){
        //self.output.retornaTurmas(turmas: mockTurmas())
        ServicoConsultaTurmas(output: self).obterTurmas(por: ids)
    }
    
    func recuperaEscolaAssociada(id: String){
        ServicoConsultaEscolas(output: self).obterEscola(id: id)
    }
    
    func mockTurmas() -> [Turma] {
        

        let turma1 = Turma(sigla: "1A", ano: Ano(descricao: "2010"), turno: Turno(descricao: "matutino"), disciplinas: [], caminhoImagem: nil, id: "", participacoesNaTurma: Array<String>(), alunos: Array<String>())
        let turma2 = Turma(sigla: "2B", ano: Ano(descricao: "2012"), turno: Turno(descricao: "vespertino"), disciplinas: [], caminhoImagem: nil, id: "", participacoesNaTurma: Array<String>(), alunos: Array<String>())
        let turma3 = Turma(sigla: "3C", ano: Ano(descricao: "2014"), turno: Turno(descricao: "noturno"), disciplinas: [], caminhoImagem: nil, id: "", participacoesNaTurma: Array<String>(), alunos: Array<String>())
        let turma4 = Turma(sigla: "1B", ano: Ano(descricao: "2018"), turno: Turno(descricao: "noturno"), disciplinas: [], caminhoImagem: nil, id: "", participacoesNaTurma: Array<String>(), alunos: Array<String>())
        
        return [turma1, turma2, turma3, turma4]
    }
}

extension ListaTurmasPresenter: ServicoConsultaTurmasOutput {
    func obterTurmas(turmas: [Turma]) {
        self.output.retornaTurmas(turmas: turmas.sorted {$0.sigla < $1.sigla})
    }
    func obterTurma(turma: Turma) {
        
    }
}

extension ListaTurmasPresenter: ServicoConsultaEscolasOutput {
    func obterEscolas(escolas: [Escola]) {}
    
    func obterEscola(escola: Escola) {
        self.output.retornaEscolaAssociada(escola: escola)
    }
}
