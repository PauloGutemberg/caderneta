//
//  FormularioSelecionaGeneroPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 30/12/20.
//

import Foundation

class FormularioSelecionaGeneroPresenter: NSObject {
    
    var output: FormularioSelecionaGeneroOutput
    
    init(output: FormularioSelecionaGeneroOutput) {
        self.output = output
    }
    
    func recuperaGeneros(){
        output.retornaGeneros(generos: mockGeneros())
    }
    
    func mockGeneros() -> [Genero] {
        let genero1 = Genero(descricao: "MASCULINO", caminhoImagem: "menino")
        let genero2 = Genero(descricao: "FEMININO", caminhoImagem: "menina")
        return [genero1, genero2]
    }
    
}
