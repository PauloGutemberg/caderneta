//
//  FormularioSelecionaTempoNotaPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/01/21.
//

import Foundation

class FormularioSelecionaTempoNotaPresenter: NSObject {
    
    var output: FormularioSelecionaTempoNotaOutput
    var periodo: Periodo
    var seguimentos: [TempoNota]?
    
    init(output: FormularioSelecionaTempoNotaOutput, periodo: Periodo){
        self.output = output
        self.periodo = periodo
    }
    
    func recuperaTempoNotas(aluno: Aluno, turma: Turma, disciplina: Disciplina){
        self.seguimentos = mockTempoNotas()
        ServicoConsultaParticipacaoTurma(output: self).obterParticipacao(idTurma: turma.id, idAluno: aluno.id, idDisciplina: disciplina.id)
    }
    
    func mockTempoNotas() -> [TempoNota] {
        let tempoNota1 = TempoNota(id: 1, descricao: SeguimentoNotaDescricao.primeiro.rawValue, estaSelecionado: false)
        let tempoNota2 = TempoNota(id: 2, descricao: SeguimentoNotaDescricao.segundo.rawValue, estaSelecionado: false)
        let tempoNota3 = TempoNota(id: 3, descricao: SeguimentoNotaDescricao.terceiro.rawValue, estaSelecionado: false)
        let tempoNota4 = TempoNota(id: 4, descricao: SeguimentoNotaDescricao.quarto.rawValue, estaSelecionado: false)
        let tempoNota5 = TempoNota(id: 5, descricao: SeguimentoNotaDescricao.recuperacao.rawValue, estaSelecionado: false)
        return [tempoNota1, tempoNota2, tempoNota3, tempoNota4, tempoNota5]
    }
    func verificaSeguimentoNota(notas: [Nota]){
        guard var seguimentos = self.seguimentos else { return }
        for nota in notas {
            if nota.periodo == "\(periodo.id)" {
                switch nota.seguimento {
                case "1":
                    seguimentos.remove(at: 0)
                case "2":
                    seguimentos.remove(at: 1)
                case "3":
                    seguimentos.remove(at: 2)
                case "4":
                    seguimentos.remove(at: 3)
                case "5":
                    seguimentos.remove(at: 4)
                default:
                    print("Nada há fazer por enquanto")
                }
            }
        }
        self.output.retornaTempoNota(tempoNotas: seguimentos)
    }
}

extension FormularioSelecionaTempoNotaPresenter: ServicoConsultaParticipacaoTurmaOutput {
    func obterParticipacoes(participacoes: [ParticipacaoNaTurma]) {}
    func obterParticipacao(participacao: ParticipacaoNaTurma) {
        ServicoConsultaNota(output: self).obterNotas(participacao: participacao)
    }
}

extension FormularioSelecionaTempoNotaPresenter: ServicoConsultaNotaOutput {
    func obterNotas(notas: [Nota]) {
        verificaSeguimentoNota(notas: notas)
    }
}
