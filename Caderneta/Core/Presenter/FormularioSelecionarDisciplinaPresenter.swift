//
//  FormularioSelecionarDisciplinaPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/12/20.
//

import Foundation


class FormularioSelecionarDisciplinaPresenter: NSObject {
    
    var output: FormularioSelecionarDisciplinaOutput
    init(output: FormularioSelecionarDisciplinaOutput) {
        self.output = output
    }
    
    func recuperaDisciplinas(){
        //output.retornaDisciplinas(disciplinas: mockDisciplinas())
        ServicoConsultaDisciplinas(output: self).obterDisciplinas()
    }
    
    ///se não houver disciplinas ele recupera do mock e adiciona um Id para cada.
    func mockDisciplinas() -> [Disciplina] {
        let disc1 = Disciplina(nome: "LING. PORTUGUESA", estaSelecionada: false, id: UUID().uuidString)
        let disc2 = Disciplina(nome: "LING. INGLESA", estaSelecionada: false, id: UUID().uuidString)
        let disc3 = Disciplina(nome: "MATEMÁTICA", estaSelecionada: false, id: UUID().uuidString)
        let disc4 = Disciplina(nome: "CIÊNCIAS BIOLÓGICAS", estaSelecionada: false, id: UUID().uuidString)
        let disc5 = Disciplina(nome: "HISTÓRIA", estaSelecionada: false, id: UUID().uuidString)
        let disc6 = Disciplina(nome: "GEOGRAFIA", estaSelecionada: false, id: UUID().uuidString)
        let disc7 = Disciplina(nome: "ARTE", estaSelecionada: false, id: UUID().uuidString)
        let disc8 = Disciplina(nome: "EDUCAÇÃO FÍSICA", estaSelecionada: false, id: UUID().uuidString)
        let disc9 = Disciplina(nome: "ENSINO RELIGIOSO", estaSelecionada: false, id: UUID().uuidString)
        let disc10 = Disciplina(nome: "FILOSOFIA", estaSelecionada: false, id: UUID().uuidString)
        let disc11 = Disciplina(nome: "FÍSICA", estaSelecionada: false, id: UUID().uuidString)
        let disc12 = Disciplina(nome: "QUÍMICA", estaSelecionada: false, id: UUID().uuidString)
        let disc13 = Disciplina(nome: "INFORMÁTICA", estaSelecionada: false, id: UUID().uuidString)
        let disc14 = Disciplina(nome: "SOCIOLOGIA", estaSelecionada: false, id: UUID().uuidString)
        return [disc1, disc2, disc3, disc4, disc5, disc6, disc7, disc8, disc9, disc10, disc11, disc12, disc13, disc14]
    }
}

extension FormularioSelecionarDisciplinaPresenter: ServicoConsultaDisciplinasOutput {
    func obterDisciplina(disciplina: Disciplina) {}
    
    func obterDisciplinas(disciplinas: [Disciplina]) {
        if disciplinas.isEmpty {
            let disciplinasDoMock = mockDisciplinas()
            disciplinasDoMock.forEach { (disciplinaMock) in
                ServicoConsultaDisciplinas(output: self).inserirDisciplina(disciplina: disciplinaMock)
            }
            recuperaDisciplinas()
        }else{
            output.retornaDisciplinas(disciplinas: disciplinas.sorted {$0.nome < $1.nome})
        }
    }
}
