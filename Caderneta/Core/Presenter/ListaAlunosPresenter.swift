//
//  ListaAlunosPresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 22/12/20.
//

import Foundation

class ListaAlunosPresenter: NSObject {
    
    var output: ListaAlunosOutput
    
    init(output: ListaAlunosOutput){
        self.output = output
    }
    
    func recuperaAlunos(ids: [String]){
        //self.output.retornaAlunos(alunos: mockAlunos())
        ServicoConsultaAlunos(output: self).obterAlunos(por: ids)
    }
    
    func recuperaTurmaAssociada(id: String){
        ServicoConsultaTurmas(output: self).obterTurma(id: id)
    }
    
    func mockAlunos() -> [Aluno] {
        let aluno1 = Aluno(nome: "João Paulo Lopes", dataNascimento: "31/11/1989", genero: "MASCULINO", caminhoImagem: nil, id: "")
        let aluno2 = Aluno(nome: "Luiz Yan", dataNascimento: "24/08/2004", genero: "MASCULINO", caminhoImagem: nil, id: "")
        let aluno3 = Aluno(nome: "Ana Maria de Sousa", dataNascimento: "01/09/1976", genero: "FEMININO", caminhoImagem: nil, id: "")
        return [aluno1, aluno2, aluno3]
    }
}

extension ListaAlunosPresenter: ServicoConsultaAlunosOutput {
    func obterAlunos(alunos: [Aluno]) {
        self.output.retornaAlunos(alunos: alunos.sorted {$0.nome < $1.nome})
    }
}

extension ListaAlunosPresenter: ServicoConsultaTurmasOutput {
    func obterTurmas(turmas: [Turma]) {}
    
    func obterTurma(turma: Turma) {
        self.output.retornaTurmaAssociada(turma: turma)
    }
}
