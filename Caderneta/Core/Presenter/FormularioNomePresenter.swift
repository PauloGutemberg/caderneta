//
//  FormularioNomePresenter.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/01/21.
//

import Foundation

class FormularioNomePresenter: NSObject {
    
    var output: FormularioNomeOutput
    
    init(output: FormularioNomeOutput){
        self.output = output
    }
    
    func inserirAluno(aluno: Aluno){
        ServicoConsultaAlunos(output: self).inserirAluno(aluno: aluno)
    }
    
    func atualizaTurmaComNovoAluno(turma: Turma){
        ServicoConsultaTurmas(output: self).atualizaTurma(turma: turma)
    }
    
    func adicionarParticipacaoAlunoNaTurma(participacao: ParticipacaoNaTurma){
        ServicoConsultaParticipacaoTurma(output: self).inserirParticipacao(participacao: participacao)
    }
}

extension FormularioNomePresenter: ServicoConsultaAlunosOutput {
    func obterAlunos(alunos: [Aluno]) {}
}

extension FormularioNomePresenter: ServicoConsultaTurmasOutput {
    func obterTurmas(turmas: [Turma]) {}
    func obterTurma(turma: Turma) {}
}

extension FormularioNomePresenter: ServicoConsultaParticipacaoTurmaOutput {
    func obterParticipacoes(participacoes: [ParticipacaoNaTurma]){}
    func obterParticipacao(participacao: ParticipacaoNaTurma){}
}
