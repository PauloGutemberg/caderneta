//
//  ServicoConsultaTurmas.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 13/01/21.
//

import Foundation

class ServicoConsultaTurmas {
    
    var output: ServicoConsultaTurmasOutput
    
    init(output: ServicoConsultaTurmasOutput) {
        self.output = output
    }
    
    func obterTurmas(por ids: [String]){
        var turmas: [Turma] = []
        for id in ids {
            let turma = TurmaProvedor().obter(id)
            if let turma = turma {
                turmas.append(turma)
            }
        }
        self.output.obterTurmas(turmas: turmas)
    }
    
    func obterTurma(id: String){
        let turma = TurmaProvedor().obter(id)
        if let turma = turma {
            self.output.obterTurma(turma: turma)
        }
    }
    
    func inserirTurma(turma: Turma){
        TurmaProvedor.init().inserir(turma)
    }
    
    func atualizaTurma(turma: Turma){
        TurmaProvedor().atualizar(turma)
    }
}
