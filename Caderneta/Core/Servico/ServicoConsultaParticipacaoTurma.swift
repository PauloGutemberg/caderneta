//
//  ServicoConsultaParticipacaoTurma.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 20/01/21.
//

import Foundation

class ServicoConsultaParticipacaoTurma {
    
    var output: ServicoConsultaParticipacaoTurmaOutput

    init(output: ServicoConsultaParticipacaoTurmaOutput) {
        self.output = output
    }
    
    func obterParticipacoes(){
        let participacoes = ParticipacaoTurmaProvedor().valores
        self.output.obterParticipacoes(participacoes: participacoes)
    }
    
    func obterParticipacao(idTurma: String, idAluno: String, idDisciplina: String){
       let participacao = ParticipacaoTurmaProvedor().obter(idTurma: idTurma, idDisciplina: idDisciplina, idAluno: idAluno)
        if let participacao = participacao {
            self.output.obterParticipacao(participacao: participacao)
        }
    }
    
    func inserirParticipacao(participacao: ParticipacaoNaTurma){
        ParticipacaoTurmaProvedor().inserir(participacao)
    }
    
    func atualizaParticipacao(participacao: ParticipacaoNaTurma){
        ParticipacaoTurmaProvedor().atualizar(participacao)
    }
    
}
