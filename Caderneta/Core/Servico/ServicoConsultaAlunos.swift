//
//  ServicoConsultaAlunos.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 19/01/21.
//

import Foundation

class ServicoConsultaAlunos {
    
    var output: ServicoConsultaAlunosOutput
    
    init(output: ServicoConsultaAlunosOutput) {
        self.output = output
    }
    
    func obterAlunos(por ids: [String]){
        var alunos: [Aluno] = []
        for id in ids {
            let aluno = AlunoProvedor().obter(id)
            if let aluno = aluno {
                alunos.append(aluno)
            }
        }
        self.output.obterAlunos(alunos: alunos)
    }
    
    func inserirAluno(aluno: Aluno){
        AlunoProvedor().inserir(aluno)
    }
}
