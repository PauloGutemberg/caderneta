//
//  ServicoConsultaDisciplinas.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 17/01/21.
//

import Foundation

class ServicoConsultaDisciplinas {
    
    var output: ServicoConsultaDisciplinasOutput
    
    init(output: ServicoConsultaDisciplinasOutput) {
        self.output = output
    }
    
    func obterDisciplinas(){
        let disciplinas = DisciplinaProvedor.init().valores
        output.obterDisciplinas(disciplinas: disciplinas)
    }
    
    func obterDisciplinas(por ids: [String]){
        var disciplinas: [Disciplina] = []
        for id in ids {
            let disciplina = DisciplinaProvedor().obter(id)
            if let disciplina = disciplina {
                disciplinas.append(disciplina)
            }
        }
        self.output.obterDisciplinas(disciplinas: disciplinas)
    }
    
    func obterDisciplina(id: String){
        if let disciplina = DisciplinaProvedor().obter(id) {
            self.output.obterDisciplina(disciplina: disciplina)
        }
    }
    
    func inserirDisciplina(disciplina: Disciplina){
        DisciplinaProvedor.init().inserir(disciplina)
    }
}
