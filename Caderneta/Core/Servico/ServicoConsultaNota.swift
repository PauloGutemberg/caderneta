//
//  ServicoConsultaNota.swift
//  Caderneta
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 21/01/21.
//

import Foundation

class ServicoConsultaNota {
    
    var output: ServicoConsultaNotaOutput
    
    init(output: ServicoConsultaNotaOutput) {
        self.output = output
    }
    
    func inserirNota(nota: Nota){
        NotaProvedor().inserir(nota)
    }
    
    func obterNotas(participacao: ParticipacaoNaTurma){
        var notas: [Nota] = []
        for id in participacao.primeiroPeriodo {
            let nota = NotaProvedor().obter(id)
            if let nota = nota {
                notas.append(nota)
            }
        }
        
        for id in participacao.segundoPeriodo {
            let nota = NotaProvedor().obter(id)
            if let nota = nota {
                notas.append(nota)
            }
        }
        
        for id in participacao.terceiroPeriodo {
            let nota = NotaProvedor().obter(id)
            if let nota = nota {
                notas.append(nota)
            }
        }
        
        for id in participacao.quartoPeriodo {
            let nota = NotaProvedor().obter(id)
            if let nota = nota {
                notas.append(nota)
            }
        }
        
        for id in participacao.recuperacao {
            let nota = NotaProvedor().obter(id)
            if let nota = nota {
                notas.append(nota)
            }
        }
        
        self.output.obterNotas(notas: notas)
    }
}
