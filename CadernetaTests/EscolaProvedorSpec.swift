//
//  EscolaProvedorSpec.swift
//  CadernetaTests
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 09/02/21.
//

import XCTest
import Foundation
import RealmSwift

@testable import Caderneta

class EscolaProvedorSpec: XCTestCase {
    
    var sut: EscolaProvedor!  //sistema em teste (termo)
    
    override func setUp() {
        super.setUp()
        
        var configuracao = Realm.Configuration.defaultConfiguration
        configuracao.inMemoryIdentifier =  "escola-provedor-spec"
        let fonte = try! Realm(configuration: configuracao)
        self.sut = EscolaProvedor(fonte: fonte)
    }
    
    override func tearDown() {
        super.tearDown()
        self.sut.valores = []
    }
    
    func testValores(){ //colocar o test (como prefixo antes do nome de qualquer funcao)
    
        //assercao (verificar aquilo que eu espero)
        XCTAssertEqual(self.sut.valores.count, 0)
        
        let escola = Escola(nome: "U.E Guilherme Guimaraes", caminhoImagem: nil, id: UUID().uuidString, turmas: [])
        self.sut.valores = [escola]
        XCTAssertEqual(self.sut.valores.count, 1)
        
    }

    func testGaranteDtearDown(){
        
        XCTAssertEqual(self.sut.valores.count, 0)
    }
    
    func testInserir(){
        let escola = Escola(nome: "U.E Guilherme Guimaraes", caminhoImagem: nil, id: UUID().uuidString, turmas: [])
        self.sut.inserir(escola)
        XCTAssertEqual(self.sut.valores.count, 1)
    }
    
    
//    func inserir(_ object: T)
//    func deletar(_ object: T)
//    func atualizar(_ object: T)
//    func obter(_ withID: String) -> T?

}
